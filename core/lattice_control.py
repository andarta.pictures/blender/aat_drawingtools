# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import numpy as np
import math
from mathutils import Vector

from ..common.mode_manager import switch_mode
from ..common.geometry_3D import to_local, get_2D_bounding_box, print_m


def assign_vertex_group(obj, vg_name, delete=False):
    """Creates a vertex group and assigns it to selected geometry.
    
    - vg_name : name of the group. If name already is assigned to a vertex group, it will be deleted.
    - delete : if True, doesn't create a vertex group and just deletes existing VG with the name vg_name.

    Returns : a bpy.types.VertexGroups object"""

    ## create vertex group
    vg = obj.vertex_groups.get(vg_name)
    if vg:
        # remove to start clean
        obj.vertex_groups.remove(vg)
    if delete:
        return

    vg = obj.vertex_groups.new(name=vg_name)
    bpy.ops.gpencil.vertex_group_assign()
    return vg


def apply_modifier(obj, modifier_name) :
    aliases = [o for o in bpy.data.objects if o.data is not None and o.data.name == obj.data.name and o.name != obj.name]

    obj.data = obj.data.copy()
    depsgraph = bpy.context.evaluated_depsgraph_get()
    depsgraph.update()

    bpy.ops.object.gpencil_modifier_apply({'object': obj}, apply_as='DATA', modifier=modifier_name)

    for alias in aliases :
        alias.data = obj.data


class LatticeControl :
    """Creates a lattice around a selection of GP_geometry. Each 
    point can then be set by worldcoordinates with set_positions().
    
    When finished, clear() or apply() transformation."""

    sort_map = None
    inverse_sort_map = None

    def __init__(self, obj) :
        """Creates a new LatticeControl object.
        
        - obj : grease pencil object to be deformed."""

        self.clear_lattice()
        self.lattices = []
        self.obj = obj
        self.gp = obj.data
        self.gp_layers = self.gp.layers
        self.view_matrix = bpy.context.space_data.region_3d.view_matrix
        self.manual_listener = False

        self.previous_mode = None

        self.lattice = None
        self.cage = None

        self.rotation_vector = None


    def create_lattice(self, dimensions=(2,2)) :
        """Create and display the lattice on screen"""

        lattice_name = 'lattice_cage_deform_%02d' % len(self.lattices)

        # Clear any preexisting lattices
        self.clear_lattice()

        # Create lattice object
        lattice = bpy.data.lattices.new(lattice_name)
        cage = bpy.data.objects.new(lattice_name, lattice)
        cage.show_in_front = True
        self.cage = cage
        self.lattice = lattice

        # Add cage to collections
        bpy.context.scene.collection.objects.link(cage)

        # spawn cage and align it to view

        r3d = bpy.context.space_data.region_3d
        viewmat = r3d.view_matrix
        self.view_matrix = viewmat

        cage.matrix_world = viewmat.inverted()

        geo = self.get_geo()
        # print_m("Geo", np.array(geo))
        x_worldsize, y_worldsize, center, centroid = get_2D_bounding_box(geo)
        self.centroid = centroid

        cage.scale = (x_worldsize, y_worldsize, 1)
        cage.location = center

        # Cage dimensions
        lattice.points_u = dimensions[0]
        lattice.points_v = dimensions[1]
        lattice.points_w = 1

        # Points interpolation mode
        lattice_interp = 'KEY_LINEAR'
        lattice.interpolation_type_u = lattice_interp
        lattice.interpolation_type_v = lattice_interp
        lattice.interpolation_type_w = lattice_interp

        # Create the GP_modifier that will do the actual transformation
        self.initialize_modifier()
        self.init_sort_map()

        # Select and make cage active
        if bpy.context.mode != 'OBJECT':
            self.previous_mode = switch_mode('OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.view_layer.objects.active = cage
        switch_mode(mode='EDIT') # go in lattice edit mode
        bpy.ops.lattice.select_all(action='SELECT') # select all points

        # Return statements
        return cage
    

    def init_sort_map(self) :
        """Creates a map of the points in the lattice sorted by X 
        then Y coordinates. Useful to find specific corners."""

        L_matrix = self.get_L_matrix()
        
        sort_map = np.lexsort((L_matrix[:,1], L_matrix[:,0])).tolist()

        inverse_sort_map = []
        for i in range(0, len(sort_map)) :
            inverse_sort_map.append(sort_map.index(i))

        self.sort_map = sort_map
        self.inverse_sort_map = inverse_sort_map
    

    def update_lattice_dimensions(self, dimensions) :
        self.lattice.points_u = dimensions[0]
        self.lattice.points_v = dimensions[1]


    def initialize_modifier(self, keep_vertex_group=False) :
        """Initializes the GP_modifier that will do the geometry transform, returns it and stores 
        it in self.gp_modifier"""

        self.clear_modifiers()

        if not keep_vertex_group :
            og_mode = switch_mode('EDIT_GPENCIL')
            vg_name = 'lattice_cage_deform_group'
            self.vertex_group = assign_vertex_group(self.obj, vg_name)
            og_mode.switch()

        mod = self.obj.grease_pencil_modifiers.new('tmp_lattice_modifier', 'GP_LATTICE')

        for _ in range(len(self.obj.grease_pencil_modifiers)):
            bpy.ops.object.gpencil_modifier_move_up(modifier='tmp_lattice_modifier')

        mod.object = self.cage
        mod.vertex_group = self.vertex_group.name

        self.gp_modifier = mod
        return mod


    def get_L_matrix(self) :
        """Return a numpy matrix with the vector coordinates of 
        all the points of the lattice, for processing and constraints purposes."""

        positions = [p.co_deform for p in self.lattice.points] # Collect position values
        positions = [np.array(p)[:,np.newaxis].T for p in positions] # Transpose to list of column vectors
        position_matrix = np.concatenate(positions) # Concatenate into a matrix

        # Sort the points to the conventionnal order
        if self.sort_map is not None :
            position_matrix = position_matrix[self.sort_map, :]

        return position_matrix


    def set_L_matrix(self, L_matrix) :
        """Update the positions of the lattice points from the lattice matrix position (cf get_L_matrix)
        - L_matrix : np.array containing the positions of every point in the lattice (generally given by get_L_matrix and then modified) 
        """

        # Sort points back to API order
        L_matrix = L_matrix[self.inverse_sort_map, :] 

        for i in range(0,len(self.lattice.points)) :
            new_vector = L_matrix[i,:]
            new_vector = Vector(new_vector.tolist())
            self.lattice.points[i].co_deform = new_vector

        
    def get_geo(self) :
        """Returns a list of points coordinates in the geometry of selected GP elements."""
        coords = []
        
        for l in self.gp_layers :
            if l.lock or l.hide or not l.active_frame :
                continue
            if self.gp.use_multiedit:
                target_frames = [f for f in l.frames if f.select]
            else:
                target_frames = [l.active_frame]

            for f in target_frames:
                for s in f.strokes:
                    if not s.select:
                        continue
                    for p in s.points:
                        if p.select:
                            # get real location
                            coords.append(self.obj.matrix_world @ p.co)
        
        return coords
    
    
    def set_positions(self, points) :
        """Set position of lattice points.
        
        - points : a (N,3) np.array containing world coordinates of each points."""

        local_points = to_local(points, self.cage.matrix_world)
        self.set_L_matrix(local_points)
    

    def get_selected_lattice_points(self) :
        """Returns the index of selected points in self.lattice.points."""

        return [i for i in range(0,len(self.lattice.points)) if self.lattice.points[i].select]
    

    def select_all_points(self) :
        """Selects all points in the lattice. Duh."""

        for p in self.lattice.points :
            p.select = True


    def apply(self) :
        """Applies GP_modifier to bake transform into the geometry."""
        apply_modifier(self.obj, self.gp_modifier.name)
        self.clear()


    def clear_lattice(self) :
        """Clears any existing transformer lattice."""
        for obj in bpy.context.scene.objects:
            if obj.name.startswith("lattice_cage_deform"):
                bpy.data.objects.remove(obj)
    

    def clear_modifiers(self) :
        """Clears GP_modifier to revert back to start geometry."""
        modifiers = self.obj.grease_pencil_modifiers
        for m in modifiers : 
            print(m.name)
            if "tmp_lattice_modifier" in m.name :
                modifiers.remove(m)


    def clear(self) :
        """Clears lattice and modifier."""
        self.clear_lattice()
        self.clear_modifiers()