# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import gpu
from gpu_extras.batch import batch_for_shader
import numpy as np
from bpy_extras import view3d_utils
import math

from ..common import geometry_3D as geo3D

class Envelope : 
    """Drawable object that will draw a line between set of handles."""

    def __init__(self, handles) :
        """- handles : list of handles to draw a line between"""
        self.handles = handles
        self.basic_color = (0.0, 0.0, 0.0, 0.7)
    
    def get_trace(self) :
        """Returns drawing data for the handle as a list of (x,y) tuples."""
        trace = []
        for h in self.handles :
            trace.append(h.position)
        trace.append(self.handles[0].position)
            
        return trace
    
    def draw(self, op, context) :
        """Draw method"""
        shader = gpu.shader.from_builtin('UNIFORM_COLOR')
        gpu.state.blend_set('ALPHA')
        gpu.state.line_width_set(2.0)
        batch = batch_for_shader(shader, 'LINE_STRIP', {"pos": self.get_trace()})
        shader.uniform_float("color", self.basic_color)
        batch.draw(shader)

        # restore opengl defaults
        gpu.state.line_width_set(1.0)
        gpu.state.blend_set('NONE')





class Handle :

    def __init__(self, position) :
        """Creates handle at given viewport position."""
        self.position = position
        self.size = 5

        self.basic_color = (0.0, 0.0, 0.0, 0.7)
        self.highlight_color = (0.9, 0.45, 0.0, 0.7)

        self.grabbed = False
        self.visible = True

    def get_color(self) :
        """Color in which to draw the handle depending on selection state."""
        if self.grabbed :
            return self.highlight_color
        else :
            return self.basic_color
    
    def in_hitbox(self, mouse_position) :
        """Return true if mouse contained in element hitbox
        - mouseposition : position of the mouse as a (x,y) tuple"""

        mx, my = mouse_position
        x,y = self.position
        size = self.size
        if x-size < mx and mx < x+size :
            if y-size < my and my < y+size :
                return True
        return False  

    def set_position(self, position) :
        """Sets position of the object. Preferable to directly accessing Handle.position
        
        - position : (x,y) tuple containing viewport position of the handle"""

        self.position = position
    
    def get_position(self) :
        """Sets position of the object. Preferable to directly accessing Handle.position"""
        return self.position
    
    def set_visible(self, visible) :
        """Sets visibility of handle. Won't be draw if visible is False."""
        self.visible = visible

    def get_trace(self) :
        """Returns drawing data for the handle as a list of (x,y) tuples."""
        mx, my = self.position
        size = self.size
        trace = [
            (mx-size, my+size), 
            (mx+size, my+size), 
            (mx+size, my-size), 
            (mx-size, my-size),
            (mx-size, my+size), 
            ]
        return trace

    def draw(self, op, context):
        """Draw method"""

        shader = gpu.shader.from_builtin('UNIFORM_COLOR')
        gpu.state.blend_set('ALPHA')
        gpu.state.line_width_set(2.0)

        batch = batch_for_shader(shader, 'LINE_STRIP', {"pos": self.get_trace()})
        shader.uniform_float("color", self.get_color())

        if self.visible :
            batch.draw(shader)

        # restore opengl defaults
        gpu.state.line_width_set(1.0)
        gpu.state.blend_set('NONE')


class Handle3D (Handle) :
    """Handle inherited class that does the same thing, 
    but based on world coordinates instead of viewport."""
    
    def __init__(self, position) :
        super().__init__(position)
        self.r3d, self.region = geo3D.get_region_settings()
        self.world_pos = geo3D.region_to_location(position, (0,0,0), self.region, self.r3d)
    
    def draw(self, op, context) :
        self.position = geo3D.location_to_region(self.world_pos, self.region, self.r3d)
        super().draw(op, context)

    def set_position(self, position):
        self.world_pos = geo3D.region_to_location(position, (0,0,0), self.region, self.r3d)
        return super().set_position(position)
    
    def get_position(self):
        return geo3D.location_to_region(self.world_pos, self.region, self.r3d)


class Rays :
    """Drawable object that will draw rays emanating from a handle."""

    def __init__(self, handle, n=8, color=None, opacity=0.4) :
        """
        - handle : Handle object
        - n : number of rays to draw"""
        self.handle = handle
        self.n_rays = n
        self.base_angle = 0
        self.opacity = opacity

        if color is None :
            self.color = (0.0, 0.0, 0.0)
        else :
            self.color = color
    
    def set_n(self, n) :
        """Sets number of rays to draw."""
        self.n_rays = n

    def calculate_ray(self, angle) :
        """Returns 2 points to draw a line passing through handle at a certain angle.
        
        - angle : angle in radians of the ray"""
        u = np.array([np.cos(angle), np.sin(angle)])
        o = np.array(self.handle.position)
        p1 = o-100000*u
        p2 = o+100000*u
        return [tuple(p1.tolist()), tuple(p2.tolist())]

    def get_ray_list(self) :
        """Returns list of rays to draw depending on self.n_rays"""
        d=2.5/self.n_rays
        t=0.005
        a=100
        ray_list = []

        ray_list.append(self.calculate_ray(-self.base_angle))

        for i in range(0, 200) :
            theta = np.arctan(d*(i+math.pow(a, i*d)-1))
            
            if theta > math.pi/2-t :
                break

            r1 = self.calculate_ray(math.pi/2+theta-self.base_angle)
            r2 = self.calculate_ray(-math.pi/2-theta-self.base_angle)

            ray_list.append(r1)
            ray_list.append(r2)

        return ray_list

    def draw(self, op, context):
        """Draw method"""
        # 50% alpha, 2 pixel width line
        shader = gpu.shader.from_builtin('UNIFORM_COLOR')
        gpu.state.blend_set('ALPHA')
        gpu.state.line_width_set(1.0)

        ray_list = self.get_ray_list()
        for ray in ray_list :
            batch = batch_for_shader(shader, 'LINE_STRIP', {"pos": ray})
            shader.uniform_float("color", (self.color[0], self.color[1], self.color[2], self.opacity))
            batch.draw(shader)

        # restore opengl defaults
        gpu.state.line_width_set(1.0)
        gpu.state.blend_set('NONE')




class QuickEditWidget :

    current_active_handle = None
    conserve_aspect_ratio = False
    central_symmetry = False

    def __init__(self, context, lattice):
        """Initiates Quick Edit Widget
        - context : blender context
        - lattice : LatticeControl object
        """

        self.lattice = lattice

        self.r3d, self.region = self.get_region_settings()

        handle_positions = self.get_coord_from_lattice()

        self.original_positions = handle_positions

        self.handles = [Handle(p) for p in handle_positions]
        self.envelope = Envelope(self.handles)

        args = (self, context)

        drawables = self.handles + [self.envelope]

        self.handlers = []
        for d in drawables :
            self.handlers.append(bpy.types.SpaceView3D.draw_handler_add(d.draw, args, 'WINDOW', 'POST_PIXEL'))
        
        self.rotating = False
        self.rotate_start_position = None
        self.translating = False
        self.translate_start_position = None
    
    def get_region_settings(self) :
        """Returns region settings for storage. Useful for 
        geometry_3D.region_to_location for example.
        
        Returns : 
        - r3d : 3D region
        - region : region"""

        for a in bpy.context.screen.areas:
            if a.type == 'VIEW_3D':
                space = a.spaces.active
                r3d = space.region_3d
                region = a.regions[-1]
                return r3d, region
        

    def get_coord_from_lattice(self) :
        """Create a list of handle positions based on geometry 
        stored in LatticeControl.
        
        Returns :
        - p_handle_positions : list of 4 tuples containing 
        float viewport coordinates of handles."""
        
        geo = self.lattice.get_geo()

        # Convert point to viewport space
        local_geo = np.array(
            [view3d_utils.location_3d_to_region_2d(
                self.region,
                self.r3d,
                v) for v in geo]
                )
        
        # Get maxima and minima on X and Y axis of the cluster point
        u,v = local_geo.T
        min_x = u.min()
        max_x = u.max()
        min_y = v.min()
        max_y = v.max()

        # Use those values as coordinates for the bounding box
        p_handle_positions = [
            (min_x, min_y),
            (min_x, max_y),
            (max_x, max_y),
            (max_x, min_y),
        ]

        return p_handle_positions
    

    def update_lattice(self) :
        """Update LatticeControl with current handle positions."""
        
        handle_pos_3D = self.get_3D_handle_positions()

        # Sort lattice points to conventionnal order
        handle_pos_3D = handle_pos_3D[(0,1,3,2), :] 

        self.lattice.set_positions(handle_pos_3D) 


    def get_handle(self, name) :
        """Get handle based on current active handle. 
        
        - name : string designating handle ('active', 'neighbor1', 'opposite', 'neighbor2')
        
        Returns Handle object"""

        if self.current_active_handle == None :
            return None
        
        relations = {"active":0, "neighbor1":1, "opposite":2, "neighbor2":3}

        return self.handles[(self.current_active_handle+relations[name])%4]


    def orthogonalize_neighbors(self, direction) :
        """Calculates position of neighbor points based 
        on active point position and orientation vector.
        
        - direction : 1D np.array vector used to 
        infer orientation of the widget. """

        a = self.get_handle("active")
        n1 = self.get_handle("neighbor1")
        n2 = self.get_handle("neighbor2")
        o = self.get_handle("opposite")

        # Diagonal vector
        d = np.array(o.position) - np.array(a.position)

        rot90 = np.array([
            [0,-1],
            [1, 0],
        ])

        # Create a (u,v) vector basis. u = O-N1, v is u rotated 90°
        # u = np.array(o.position) - np.array(n1.position)
        u = direction / np.linalg.norm(direction)
        v = rot90 @ u

        basis = np.stack([u,v])

        # Decompose diagonal into its u and v components d_u and d_v 
        # such that d = d_u*u + d_v*v
        decompo = basis @ d

        # N1 = O+d_u*u
        # N2 = O+d_v*v
        new_n1 = np.array(o.position) - u*decompo[0]
        new_n2 = np.array(o.position) - v*decompo[1]

        # Update neighbors to their new positions
        n1.position = (new_n1[0], new_n1[1])
        n2.position = (new_n2[0], new_n2[1])
    
    def get_active_handle(self) :
        """Returns index of currently selected handle, None if there is none."""
        
        for h in self.handles : 
            if h.grabbed : 
                return self.handles.index(h)
        return None


    def update_handle_positions(self, mouse_position) :
        """Function called with every mouse movement to update the handles according to mouse information.
        
        - mouse_position : (x,y) position of the mouse as found in modal informations"""

        self.current_active_handle = self.get_active_handle()

        if self.current_active_handle is None :
            return False
        
        a = self.get_handle("active")
        n1 = self.get_handle("neighbor1")
        n2 = self.get_handle("neighbor2")
        o = self.get_handle("opposite")

        pivot = geo3D.middle_point(np.array(a.position), np.array(o.position))
        d = np.array(o.position) - np.array(a.position)

        # Store orthogonal direction
        u = np.array(o.position) - np.array(n1.position)

        # If conserve_aspect_ratio is activated, constrain position of active handle accordingly
        if self.conserve_aspect_ratio :
            dn = geo3D.normalize_vector(d)
            p = np.array(mouse_position) - pivot
            p_constrained = np.dot(p,dn)*dn
            pos_a = pivot + p_constrained
            a.position = tuple(pos_a.tolist())
        else :
            a.position = mouse_position

        # If central_symmetry is activated, constrain opposite handle accordingly
        if self.central_symmetry :
            v = np.array(a.position) - pivot
            pos_o = pivot - v
            o.position = tuple(pos_o.tolist())


        # Infer position of neighbor handles
        self.orthogonalize_neighbors(u)

        # Transfer handle information to the lattice
        self.update_lattice()

        return True


    def rotate(self, mouse_position) :
        """Rotate points according to mouse position. Called from modal.

        - mouse_position : (x,y) position of the mouse as found in modal informations
        """

        p1 = np.array(self.handles[0].position)
        p2 = np.array(self.handles[2].position)

        pivot = geo3D.middle_point(p1, p2)

        u = np.array(self.rotate_start_position) - pivot
        v = np.array(mouse_position) - pivot

        angle = geo3D.oriented_2D_angle(u,v)

        if np.isnan(angle) :
            return

        # Rotation matrix in 2D space
        rot = np.array([
            [np.cos(angle), -np.sin(angle)], 
            [np.sin(angle),  np.cos(angle)]
        ])

        for h in self.handles :
            w = np.array(h.position) - pivot
            w = rot @ w
            new_pos = w + pivot
            h.position = (new_pos[0], new_pos[1])
        
        self.rotate_start_position = mouse_position

        self.update_lattice()

    def translate(self, mouse_position) :
        """Translate points according to mouse position. Called from modal.

        - mouse_position : (x,y) position of the mouse as found in modal informations
        """

        t = np.array(mouse_position) - np.array(self.translate_start_position)

        for h in self.handles : 
            p = h.position
            h.position = (p[0]+t[0], p[1]+t[1])
        
        self.translate_start_position = mouse_position

        self.update_lattice()

    def measure_rotation(self) :
        """Return rotation of widget in radians."""
        u = np.array(self.handles[1].position) - np.array(self.handles[0].position)
        v = np.array(self.original_positions[1]) - np.array(self.original_positions[0])
        angle = geo3D.oriented_2D_angle(u,v)
        return angle
    
    
    def get_3D_handle_positions(self) :
        """Returns a list of all handles positions in world space."""
        positions = []
        for h in self.handles : 
            pos3D = geo3D.region_to_location(h.position, self.lattice.centroid, self.region, self.r3d)
            positions.append(np.array(pos3D))
        return np.stack(positions)
    
    
    def inside_widget(self, pos) :
        """Returns True if pos is currently in the widget, False if it's outside."""

        a = self.handles[0]
        n1 = self.handles[1]
        n2 = self.handles[3]

        u = np.array(n1.position) - np.array(a.position)
        v = np.array(n2.position) - np.array(a.position)
        un = geo3D.normalize_vector(u)
        vn = geo3D.normalize_vector(v)
        p = np.array(pos) - np.array(a.position)

        u_composite = np.dot(un,p)/np.linalg.norm(u)
        v_composite = np.dot(vn,p)/np.linalg.norm(v)

        return 0 < u_composite and u_composite < 1 and 0 < v_composite and v_composite < 1


    def on_modal(self, context, event) :
        """Must be called from the modal() function of the operator to make widget responsive.
        
        - context : blender local context
        - event : current event as passed to modal()"""

        mouse_position = (event.mouse_region_x, event.mouse_region_y)
        
        if event.type == 'MOUSEMOVE':
            if self.rotating :
                self.rotate(mouse_position)
            elif self.translating :
                self.translate(mouse_position)
            else :
                self.update_handle_positions(mouse_position)
        
        if event.type == "LEFTMOUSE" and event.value == "PRESS" : 
            handle_grabbed = False
            for h in self.handles : 
                if h.in_hitbox(mouse_position) : 
                    h.grabbed = True
                    handle_grabbed = True
            if not handle_grabbed :
                if self.inside_widget(mouse_position) : 
                    self.translating = True
                    self.translate_start_position = mouse_position
                else :
                    self.rotating = True
                    self.rotate_start_position = mouse_position
        
        if event.type == "LEFTMOUSE" and event.value == "RELEASE" : 
            for h in self.handles :
                h.grabbed = False
            self.rotating = False
            self.translating = False
            self.rotate_start_position = None
    
    def delete(self) :
        """Deletes widget and unregisters it from opengl."""
        for hs in self.handlers : 
            bpy.types.SpaceView3D.draw_handler_remove(hs, 'WINDOW')




class PerspectiveGrid :
    """Widget displaying a 2 point perspective grid on screen."""

    def __init__(self, context) :

        self.keep_horizontal = False
        self.active = True
        self.visible = True
        self.context = context

        vw, vh = geo3D.get_viewport_width_height()

        self.r3d, self.region = self.get_region_settings()

        self.current_active_handle = None

        handle_positions = [
            (int(vw/2-100), int(vh/2)), 
            (int(vw/2+100), int(vh/2)), 
        ]

        self.handles = [Handle3D(p) for p in handle_positions]

        self.ray_n = 8

        pink = (168/255, 37/255, 130/255)
        green = (49/255, 168/255, 37/255)
        opacity = 0.4
        self.rays = [Rays(self.handles[0], self.ray_n, pink, opacity), Rays(self.handles[1], self.ray_n, green, opacity)]

        self.register_drawables(self.handles + self.rays)

    def register_drawables(self, drawables) :

        args = (self, self.context)
        
        self.handlers = []
        for d in drawables :
            self.handlers.append(bpy.types.SpaceView3D.draw_handler_add(d.draw, args, 'WINDOW', 'POST_PIXEL'))
    
    def get_region_settings(self) :
        """Returns region settings for storage. Useful for 
        geometry_3D.region_to_location for example.
        
        Returns : 
        - r3d : 3D region
        - region : region"""

        for a in bpy.context.screen.areas:
            if a.type == 'VIEW_3D':
                space = a.spaces.active
                r3d = space.region_3d
                region = a.regions[-1]
                return r3d, region
    
    def update_ray_angle(self) :
        """Updates the angle of the horizon based on position of two perspective points."""

        u = np.array(self.handles[1].position) - np.array(self.handles[0].position)
        v = np.array([1,0])
        theta = geo3D.oriented_2D_angle(u,v)
        for r in self.rays :
            r.base_angle = theta
    
    def set_ray_n(self, n) :
        """Sets number of ray around each perspective point."""
        self.ray_n = n
        for r in self.rays :
            r.set_n(n)
    
    def get_ray_n(self) :
        """Returns number of ray around each perspective point."""
        return self.ray_n
    
    def get_opacity(self) :
        return self.rays[0].opacity

    def set_opacity(self, opacity) :
        for r in self.rays :
            r.opacity = opacity
    
    def set_active(self, active) :
        """Sets mode for perspective grid. Active means you can modify the 
        grid by its points, not active means it's non modifiable overlay.
        
        - active : boolean"""

        self.active = active

        if self.active : 
            for h in self.handles :
                h.set_visible(True)
        else :
            for h in self.handles :
                h.set_visible(False)
                    

    def on_modal(self, context, event) :
        """Must be called from the modal() function of the operator to make widget responsive.
        
        - context : blender local context
        - event : current event as passed to modal()"""

        if not self.active :
            return

        mouse_position = (event.mouse_region_x, event.mouse_region_y)
        
        if event.type == 'MOUSEMOVE':
            self.update_handle_positions(mouse_position)
        
        if event.type == "LEFTMOUSE" and event.value == "PRESS" : 
            for h in self.handles : 
                if h.in_hitbox(mouse_position) : 
                    h.grabbed = True
                    self.current_active_handle = h
        
        if event.type == "LEFTMOUSE" and event.value == "RELEASE" : 
            for h in self.handles :
                h.grabbed = False
            
            self.current_active_handle = None
    
    def update_handle_positions(self, mouse_position) :
        """Function called with every mouse movement to update the handles according to mouse information.
        
        - mouse_position : (x,y) position of the mouse as found in modal informations"""

        if self.current_active_handle is None :
            return False
        

        if self.keep_horizontal :
            i_a = self.handles.index(self.current_active_handle)
            i_o = (i_a+1)%2
            o = self.handles[i_o]
            mouse_position = (mouse_position[0], o.get_position()[1])
        
        self.current_active_handle.set_position(mouse_position)
        self.update_ray_angle()

        return True
    
    def set_visible(self, visible) :
        
        if visible != self.visible :
            self.context.area.tag_redraw()
            if visible :
                print("Registering drawables !")
                self.register_drawables(self.handles + self.rays)
            else :
                print("Unregistering drawables.")
                self.delete()
        
        self.visible = visible
    
    def delete(self) :
        """Deletes widget and unregisters it from opengl."""
        for hs in self.handlers : 
            bpy.types.SpaceView3D.draw_handler_remove(hs, 'WINDOW')