# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import numpy as np
import mathutils
import math
import collections
import random

from ..common import gp_utils, log_3D
from ..common import timer
from ..common.geometry_3D import location_to_region, region_to_location, to_local, to_world, print_m, calculate_intersection

from . import boolean

try :
    import shapely, shapely.ops as shapely_ops
except ImportError :
    shapely_available = False
else :
    shapely_available = True


current_object = None

def get_bridges(geometry) :
    geometry = np.array(geometry)

    unq, count = np.unique(geometry, axis=0, return_counts=True)
    repeated_groups = unq[count > 1]

    repetitions = []

    for repeated_group in repeated_groups:
        repeated_idx = np.argwhere(np.all(geometry == repeated_group, axis=1))
        repetitions.append(repeated_idx.ravel().tolist())
    
    connected_repetition = []
    connected_points = []

    bridges = []

    for i in range(0, len(repetitions)) :
        for j in range(0, len(repetitions)) :
            if i != j :
                for p1 in repetitions[i] :
                    for p2 in repetitions[j] :
                        if p1+1 == p2 :
                            connected_repetition.append((i,j))
                            connected_points.append((p1, p2))
                            if (j,i) in connected_repetition :
                                p3,p4 = connected_points[connected_repetition.index((j,i))]
                                bridge = [p1, p2, p3, p4]
                                bridge.sort()
                                bridge = [bridge[0], bridge[2]]
                                bridges.append(bridge)
    
    return bridges


def stroke_to_polygons(stroke, geometry) :
    bridges = get_bridges(geometry)

    inner_rings = []

    inshape_indices = []
    for b in bridges :
        indices = [i for i in range(b[0]+1, b[1]+1)]
        inshape_indices += indices
        inshape_geometry = [geometry[i] for i in indices]
        inner_rings.append(shapely.LinearRing(inshape_geometry))

    outshape_indices = [i for i in range(0, len(geometry)) if i not in inshape_indices]
    outer_ring = [geometry[i] for i in outshape_indices]

    if len(inner_rings) > 0 :
        return shapely.polygons(outer_ring, holes=inner_rings)

    else :
        return shapely.polygons(outer_ring)
    

def display_polygon(polygon, depth_point, colors=["GREEN", "RED"]) :
    for poly in unpack_poly(polygon) :
        log_3D.line([(p[0], depth_point.y, p[1]) for p in poly.exterior.coords], color_name=colors[0])

        for interior in poly.interiors:
            log_3D.line([(p[0], depth_point.y, p[1]) for p in interior.coords], color_name=colors[1])


def get_hitbox_from_stroke(stroke, step=1) :
    min_x, min_z, max_x, max_z = None, None, None, None
    
    for i in range(0, len(stroke.points), step) :
        p = stroke.points[i].co
        if min_x is None or p.x < min_x :
            min_x = p.x
        if max_x is None or p.x > max_x :
            max_x = p.x
        if min_z is None or p.z < min_z :
            min_z = p.z
        if max_z is None or p.z > max_z :
            max_z = p.z
        
    return min_x, min_z, max_x, max_z


def hitbox_collide(hitbox1, hitbox2) :
    X_hitbox_diff = np.array(hitbox1)[[0,2]]-np.array(hitbox2)[[2,0]]
    Y_hitbox_diff = np.array(hitbox1)[[1,3]]-np.array(hitbox2)[[3,1]]

    return np.prod(X_hitbox_diff) < 0 and np.prod(Y_hitbox_diff) < 0


def stroke_to_lasso_collision(lasso_poly, strokes, gp_obj) :
    collision_status = []

    lasso_path_world = [(p[0], gp_obj.location.y, p[1]) for p in lasso_poly.exterior.coords]
    lasso_loc = to_local(lasso_path_world, gp_obj.matrix_world)
    L_hitbox = boolean.get_hitbox(lasso_loc[:,(0,2)])

    for s in strokes :
        approx_step = 5 if len(s.points) > 10 else 1
        G_hitbox = get_hitbox_from_stroke(s, approx_step)
        collision_status.append(hitbox_collide(L_hitbox, G_hitbox))
    
    return collision_status


def strokes_to_poly(strokes, gp_obj) :
    geometry = strokes_to_geometry(strokes, gp_obj)
    polys = geometry_to_polys(geometry)
    return polys


def strokes_to_geometry(strokes, gp_obj, layer=None) :
    pathes = []

    transformation_matrix = gp_obj.matrix_world

    if layer is not None :
        layer_matrix = gp_utils.get_layer_matrix(layer)
        print("Apply layer matrix : ", layer_matrix)
        transformation_matrix = transformation_matrix @ layer_matrix

    for s in strokes :
        geometry = [transformation_matrix @ p.co for p in s.points]
        geometry = [(p[0], p[2]) for p in geometry]
        pathes.append(geometry)

    return pathes


def geometry_to_polys(geometry, simplify=False, holes_allowed=True) :
    polys = []

    for g in geometry :
        g_poly = shapely.Polygon(g)
        
        if simplify :
            g_poly = g_poly.simplify(0.001)
        
        g_poly = shapely.make_valid(g_poly)

        if not holes_allowed :
            if g_poly is not None and unpack_poly(g_poly) is not None :
                subpolys = []
                for p in unpack_poly(g_poly) :
                    p = shapely.Polygon(p.exterior.coords)
                    subpolys.append(p)
                
                if len(subpolys) == 0 :
                    continue

                g_poly = shapely.MultiPolygon(subpolys) if len(subpolys) > 1 else subpolys[0]

        polys.append(g_poly)
    
    return polys


def poly_to_lasso_collision(lasso_poly, polys, gp_obj) :
    collision_status = []

    for g_poly in polys :

        intersection = shapely.intersection(lasso_poly, g_poly)
        collision = intersection.area > 0
        collision_status.append(collision)
    
    return collision_status


def cull_small_strokes(strokes, frame, n=5) :
    for i,s in enumerate(strokes) :
        if len(s.points) < n :
            print("Deleting a stroke of %d elements"%len(s.points))
            frame.strokes.remove(s)
            del strokes[i]
    return strokes


def filter_materials(strokes, obj) :
    active = lambda s : s.material_index == obj.active_material_index 
    locked = lambda s : gp_utils.stroke_material(s, obj).grease_pencil.lock

    filtered = [s for s in strokes if not locked(s)]
    
    active_material_only = bpy.context.scene.restrict_bool_to_active_material
    if active_material_only :
        filtered = [s for s in filtered if active(s)]
    
    return filtered


def collect_intersecting_polygons(frame, lasso_poly, gp_obj) :

    if len(frame.strokes) == 0 :
        return [], []
    
    stroke_collision = stroke_to_lasso_collision(lasso_poly, frame.strokes, gp_obj)
    print("Strokes : %d, Collisions : %d" % (len(frame.strokes), len(stroke_collision)))
    hit_strokes = [frame.strokes[i] for i in range(0, len(frame.strokes)) if stroke_collision[i] == True]

    if len(hit_strokes) == 0 :
        return [], []
    
    hit_strokes = cull_small_strokes(hit_strokes, frame)
    hit_strokes = filter_materials(hit_strokes, gp_obj)
    polys = strokes_to_poly(hit_strokes, gp_obj)

    poly_collisions = poly_to_lasso_collision(lasso_poly, polys, gp_obj)
    result_polygons = [polys[i] for i in range(0, len(polys)) if poly_collisions[i] == True]
    result_strokes = [hit_strokes[i] for i in range(0, len(polys)) if poly_collisions[i] == True]

    return result_polygons, result_strokes


def flatten_strokes(strokes, gp_obj, r3d) :
    print("Flattening strokes")
    flatten_count = 0

    depth_point = np.array(gp_obj.location)
    view_location = np.array(r3d.view_matrix.inverted().translation)

    matrix1 = mathutils.Matrix(gp_obj.matrix_world)
    world_mat = np.asarray(matrix1)
    mat = world_mat[:3, :3]
    loc = world_mat[:3, 3]
    inv_mat = np.linalg.inv(mat.T)

    for s in strokes :
        for p in s.points :
            if abs(p.co.y) > 0.00001 :
                world_coord = np.array(p.co) @ mat + loc
                world_coord = calculate_intersection(depth_point, np.array([0,1,0]), view_location, world_coord-view_location)
                local_coord = (world_coord - loc) @ inv_mat
                local_coord = mathutils.Vector(local_coord.tolist())
                p.co = local_coord
                flatten_count += 1
    
    print("%d points flattened." % flatten_count)


def to_local_plane(coordinates, obj, depth_point, r3d) :

    matrix1 = mathutils.Matrix(obj.matrix_world)
    world_mat = np.asarray(matrix1)
    mat = world_mat[:3, :3]
    loc = world_mat[:3, 3]
    inv_mat = np.linalg.inv(mat.T)

    location = np.array(r3d.view_matrix.inverted().translation)
    depth_point = np.array(depth_point)
    print_m("Depth point", depth_point)

    result = []
    
    for i in range(0, len(coordinates)) :
        world_coord = np.array(coordinates[i])
        world_coord = calculate_intersection(depth_point, np.array([0,1,0]), location, world_coord-location)
        local_coord = (world_coord - loc) @ inv_mat
        result.append(mathutils.Vector(local_coord.tolist()))

    return result


def get_closest_neighbors(set1, set2) :

    line1 = shapely.LineString(set1)
    line2 = shapely.LineString(set2)

    p1,p2 = shapely_ops.nearest_points(line1, line2)

    i = np.argmin(np.linalg.norm(set1-np.array([p1.x, p1.y]), axis=1))
    j = np.argmin(np.linalg.norm(set2-np.array([p2.x, p2.y]), axis=1))

    return i,j


def extrude_cavity(container, containee) :
    container_i, containee_i = get_closest_neighbors(container, containee)
    new_shape = container[:container_i+1]+containee[containee_i:]+containee[:containee_i+1]+container[container_i:]
    return new_shape


def check_for_crossing_bridges(geometry, polygon) :
    bridges = get_bridges(geometry)

    broken_holes = []

    for b in bridges : 
        bridge_ring = shapely.LineString([geometry[b[0]], geometry[b[1]]])
        for i,hole in enumerate(polygon.interiors) :
            if hole.crosses(bridge_ring) :
                broken_holes.append(i)
    
    broken_holes = list(set(broken_holes))
    return broken_holes
    
        

def compute_geometry(polygon, hole_order=None) :
    computed_shape = list(polygon.exterior.coords)

    if hole_order is None :
        hole_order = list(range(0, len(polygon.interiors)))

    for i in hole_order :
        interior = polygon.interiors[i]
        inner_ring = list(interior.coords)
        computed_shape = extrude_cavity(computed_shape, inner_ring)
    
    return computed_shape

def compute_geometry_safe(polygon, n=10) :

    hole_order = list(range(0, len(polygon.interiors)))
    
    for i in range(0,n) :
        geometry = compute_geometry(polygon, hole_order)
        broken_holes = check_for_crossing_bridges(geometry, polygon)
        print("Geometry compute, try #%d : %d broken holes."%(i, len(broken_holes)))
        if len(broken_holes) == 0 :
            return geometry
        else :
            ok_holes = [i for i in hole_order if i not in broken_holes]
            hole_order = broken_holes + ok_holes
    
    return geometry
    

def unpack_poly(polygon) :
    if type(polygon) is shapely.Polygon :
        return [polygon]

    elif type(polygon) is shapely.MultiPolygon :
        return [p for p in polygon.geoms]
    
    elif type(polygon) is shapely.GeometryCollection :
        for g in polygon.geoms :
            polys = []
            if type(g) is shapely.Polygon :
                polys.append(g)
            return polys
    
def create_new_stroke(p3d, stroke, point, frame, from_existing=None) :

    if from_existing is not None :
        new_stroke = stroke
        if len(new_stroke.points) > len(p3d) : 
            while len(new_stroke.points) > len(p3d) :
                new_stroke.points.pop()
        else :
            new_stroke.points.add(len(p3d)-len(new_stroke.points), pressure = point.pressure, strength = point.strength)
    
    else :
        new_stroke = frame.strokes.new()

        new_stroke.material_index = stroke.material_index
        new_stroke.line_width = stroke.line_width

        new_stroke.points.add(len(p3d), pressure = point.pressure, strength = point.strength)
    
    return new_stroke


def polygons_to_stroke(polygons, stroke, frame, object, recycle_stroke=True, layer=None) :

    strokes = []

    polys = unpack_poly(polygons)
    largest_poly_i = np.argmax([len(p.exterior.coords) for p in polys])

    for i,poly in enumerate(polys) :
        print_poly("Stroke poly #%d"%i, poly)
        point = stroke.points[0]

        geometry = compute_geometry_safe(poly)

        depth_ref = object.matrix_world @ stroke.points[0].co
        depth = depth_ref[1]
        p3d = [(p[0], depth, p[1]) for p in geometry]

        T_matrix = object.matrix_world
        if layer is not None :
            layer_matrix = gp_utils.get_layer_matrix(layer)
            T_matrix = T_matrix @ layer_matrix

        p3d = to_local(p3d, T_matrix)

        if recycle_stroke and i == largest_poly_i :
            new_stroke = create_new_stroke(p3d, stroke, point, frame, from_existing=stroke)
        else :
            new_stroke = create_new_stroke(p3d, stroke, point, frame)

        for i in range(0, len(p3d)) :
            new_stroke.points[i].co = p3d[i]

        strokes.append(new_stroke)
    
    return strokes


def remove_old_strokes(strokes, frame) :
    for s in strokes : 
        frame.strokes.remove(s)


def print_poly(label, poly) :
    while label[-1] in [' ', ':'] :
        label = label[:-1]

    if type(poly) is shapely.Polygon :
        n_exterior = len(poly.exterior.coords)
        n_interiors = [len(inner.coords) for inner in poly.interiors]
        print("\n[Polygon] %s : Outer ring : %d\tInner rings : %s."%(label, n_exterior, str(n_interiors)))

    elif type(poly) is shapely.MultiPolygon :
        unpacked = unpack_poly(poly)
        print("\n[MultiPolygon] %s : %d polys."%(label, len(unpacked)))

        for i,p in enumerate(unpacked) :
            n_exterior = len(p.exterior.coords)
            n_interiors = [len(inner.coords) for inner in p.interiors]
            print("#%d > Outer ring : %d\tInner rings : %s"%(i, n_exterior, str(n_interiors)))
        
    elif type(poly) is shapely.GeometryCollection :
        print("\n[GeometryCollection] %s : %d polys."%(label, len(poly.geoms)))
        for i,g in enumerate(poly.geoms) :
            print_poly("#Element %d : "%i, g)
        
    elif type(poly) is shapely.LineString :
        print("\n[LineString] %s : %d points."%(label, len(poly.coords)))

    else :
        print("\n[Unkown] %s : %s."%(label, str(type(poly))))

def geocollection_to_poly(geocollection) :
    polys = [g for g in geocollection.geoms if type(g) is shapely.Polygon]
    
    if len(polys) == 1 :
        return polys[0]
    
    elif len(polys) > 1 :
        return shapely.MultiPolygon(polys)
    

def get_path_distance(path) :
    individual_distance = np.linalg.norm(path[1:,]-path[:-1,], axis=1)
    distance = np.sum(individual_distance)
    return distance
    

def lasso_smooth(path, target_ratio=0.1) :
    lasso_array = np.array(path)

    distance = get_path_distance(lasso_array)
    print("\nPath smoothing : ")

    kernel = np.array([0.15, 0.2, 0.3, 0.2, 0.15])

    smoothed_path_x = np.convolve(lasso_array[:,0], kernel)
    smoothed_path_y = np.convolve(lasso_array[:,1], kernel)

    smoothed_path = np.array([smoothed_path_x, smoothed_path_y])
    smoothed_path = np.transpose(smoothed_path, (1,0))

    n = lasso_array.shape[0]
    # target_ratio = 0.1
    current_ratio = n/distance

    print("Path distance : %.2f, Points : %d, Ratio : %.2f" % (distance, n, n/distance))
    keep_point_ratio = int(current_ratio/target_ratio)
    keep_point_ratio = keep_point_ratio if keep_point_ratio > 0 else 1
    print("Keeping one every %d points."%keep_point_ratio)

    selection = list(range(4, smoothed_path.shape[0]-4, keep_point_ratio))
    selected_path = smoothed_path[selection,:].tolist()
    smoothed_path = [path[0]] + selected_path + [path[-1]]

    print("Smoothing : %d to %d points."%(len(path), len(smoothed_path)))
    print("Original ratio : %.2f, Target ratio : %.2f, Current ratio : %.2f"%(current_ratio, target_ratio, len(smoothed_path)/distance))

    return smoothed_path

def poly_boolean(lasso_poly, union_poly, mode) :

    unpacked = unpack_poly(union_poly)
    
    if unpacked is None or len(unpacked) == 0 :
        if mode == "ADD" :
            return lasso_poly
        else :
            return None
    
    if len(unpacked) > 0 :
        if mode == "ADD" :
            return union_poly.union(lasso_poly)
        else : 
            return union_poly.difference(lasso_poly)


def get_lasso_poly(lasso_path, gp_obj, region, r3d, depth_ref=None) :
    if depth_ref is None : 
        depth_ref = gp_obj.location
    
    smoothed_lasso_path = lasso_smooth(lasso_path)
    smoothed_lasso_path = [region_to_location(p, depth_ref, region, r3d) for p in smoothed_lasso_path]
    smoothed_lasso_path = [(p[0], p[2]) for p in smoothed_lasso_path]

    lasso_poly = shapely.Polygon(smoothed_lasso_path)
    lasso_poly = shapely.make_valid(lasso_poly)

    if type(lasso_poly) is shapely.MultiPolygon :
        largest_poly_i = np.argmax([len(poly.exterior.coords) for poly in lasso_poly.geoms])
        lasso_poly = lasso_poly.geoms[largest_poly_i]

    elif type(lasso_poly) is shapely.GeometryCollection :
        return None
    
    return lasso_poly


def apply_attenuate(lasso_path, mode) :
    
    gp_obj = gp_utils.get_active_gp_object()
    active_layer = boolean.get_active_layer(gp_obj)
    frame = active_layer.active_frame
    log_3D.destroy_3D_logs()
    region, r3d = boolean.get_view_region()

    if active_layer is None :
        return

    if len(lasso_path) < 10 :
        return
    
    flatten_strokes(frame.strokes, gp_obj, r3d)
    
    lasso_poly = get_lasso_poly(lasso_path, gp_obj, region, r3d)
    if lasso_poly is None :
        return

    polygons, strokes = collect_intersecting_polygons(frame, lasso_poly, gp_obj)

    print("Mode : ", mode)

    print(f'{len(strokes)} strokes hit by attenuate.')

    for s in strokes :
        for p in s.points : 
            if mode == "ADD" :
                p.strength *= 2
            else :
                p.strength *= 0.5



def apply_potato(lasso_path, mode) :
    
    gp_obj = gp_utils.get_active_gp_object()
    active_layer = boolean.get_active_layer(gp_obj)
    frame = active_layer.active_frame
    log_3D.destroy_3D_logs()
    region, r3d = boolean.get_view_region()

    if active_layer is None :
        return

    if len(lasso_path) < 10 :
        return
    
    flatten_strokes(frame.strokes, gp_obj, r3d)

    p_global = to_world(np.array(frame.strokes[0].points[0].co), gp_obj.matrix_world)
    depth_ref = mathutils.Vector(p_global.tolist())

    lasso_poly = get_lasso_poly(lasso_path, gp_obj, region, r3d, depth_ref=depth_ref)
    if lasso_poly is None :
        return

    polygons, strokes = collect_intersecting_polygons(frame, lasso_poly, gp_obj)

    union_poly = shapely.union_all(polygons)

    if type(union_poly) is shapely.GeometryCollection :
        union_poly = geocollection_to_poly(union_poly)

    result_poly = poly_boolean(lasso_poly, union_poly, mode)
    if result_poly is None :
        return
    
    result_is_empty = type(result_poly) is shapely.Polygon and len(result_poly.exterior.coords) == 0

    if not result_is_empty :
        if len(strokes) > 0 :
            largest_stroke_i = np.argmax([len(s.points) for s in strokes])
            largest_stroke = strokes.pop(largest_stroke_i)
        
        else :
            largest_stroke = frame.strokes.new()
            largest_stroke.material_index = gp_obj.active_material_index
            largest_stroke.points.add(1)

        polygons_to_stroke(result_poly, largest_stroke, frame, gp_obj, layer=active_layer)
    
    remove_old_strokes(strokes, frame)


def ring_area(ring) :
    b = ring.bounds
    return (b[2]-b[0])*(b[3]-b[1])


def clean_up_poly(polygon) :
    print("\nResult polygon clean-up.")
    
    clean_polys = []
    
    for poly in unpack_poly(polygon) :
        exterior = list(poly.exterior.coords)
        exterior_area = ring_area(poly.exterior)

        if exterior_area == 0.0 :
            continue

        interiors = []
        for r in poly.interiors :
            hole_area = ring_area(r)
            area_ratio = math.sqrt(hole_area/exterior_area)

            if len(r.coords) > 15 and area_ratio > 0.001 : 
                interiors.append(list(r.coords))
            else :
                print("Hole rejected : %d points, %.5f area ratio"%(len(r.coords), area_ratio))

        n_removed = len(poly.interiors) - len(interiors)
        if n_removed > 0 :
            print("%d inner holes culled."%n_removed)

        new_poly = shapely.Polygon(exterior, holes=interiors)
        clean_polys.append(new_poly)
    
    return shapely.MultiPolygon(clean_polys)
    

def get_union_polygon(frame_n, layer, gp_obj, r3d) :
    global current_object
    current_object = gp_obj

    frame = get_frame(layer, frame_n)

    flatten_strokes(frame.strokes, gp_obj, r3d)
    strokes = [s for s in frame.strokes]
    strokes = cull_small_strokes(strokes, frame)

    geometry = strokes_to_geometry(strokes, gp_obj, layer)

    polys = geometry_to_polys(geometry, simplify=True, holes_allowed=False)
    valid_polys = [p for p in polys if type(p) is shapely.Polygon or type(p) is shapely.MultiPolygon]

    union_poly = shapely.union_all(valid_polys)
    union_poly = clean_up_poly(union_poly)

    return union_poly, strokes


def is_valid_layer(layer) :
    if layer is None :
        return False

    if len(layer.active_frame.strokes) == 0 :
        return False
    
    if layer.hide == True :
        return False
    
    return True


def get_frame_list(layers) :
    if bpy.context.scene.boolean_multiframe :
        frames = []
        for layer in layers :
            frames += [f.frame_number for f in layer.frames]
        frames = list(set(frames))
        frames.sort()
        return frames
    
    else : 
        return [bpy.context.scene.frame_current]
    

def get_frame(layer, n) :
    frame_list = [f for f in layer.frames]
    frame_list = [f for f in frame_list if f.frame_number <= n]
    frame_list.sort(key=lambda f : f.frame_number)

    # print("Frames lower than %d : " % n, [f.frame_number for f in frame_list])

    if len(frame_list) == 0 :
        return None
    
    return frame_list[-1]

def delete_frame(layer, n) :
    for f in layer.frames :
        if f.frame_number == n :
            layer.frames.remove(f)
    

def layer_union(layer, gp_obj) :

    log_3D.destroy_3D_logs()
    region, r3d = boolean.get_view_region()

    if not is_valid_layer(layer) :
        return
    
    frame_list = get_frame_list([layer])

    new_polys = {}

    for f in frame_list :
        poly1, strokes1 = get_union_polygon(f, layer, gp_obj, r3d)
        new_polys[f] = (poly1, strokes1[0])

    
    for f in new_polys.keys() :
        poly, stroke = new_polys[f]
        mapping = shapely.geometry.mapping(poly)

        delete_frame(layer, f)
        new_frame = layer.frames.new(f)

        if len(mapping["coordinates"]) > 0 :
            polygons_to_stroke(poly, stroke, new_frame, gp_obj, recycle_stroke=False, layer=layer)

    for f in frame_list :
        bpy.context.scene.frame_set(f)
        boolean.refresh_geometry_display()


def layer_difference(layer1, layer2, gp_obj) :
    layer_boolean(layer1, layer2, gp_obj, shapely.difference)


def layer_intersection(layer1, layer2, gp_obj) :
    layer_boolean(layer1, layer2, gp_obj, shapely.intersection)


def layer_boolean(layer1, layer2, gp_obj, bool_function) :

    log_3D.destroy_3D_logs()
    region, r3d = boolean.get_view_region()

    if not is_valid_layer(layer1) or not is_valid_layer(layer2) :
        return
    
    frame_list = get_frame_list([layer1, layer2])

    new_polys = {}

    og_stroke = None

    for f in frame_list :
        poly1, strokes1 = get_union_polygon(f, layer1, gp_obj, r3d)
        poly2, strokes2 = get_union_polygon(f, layer2, gp_obj, r3d)

        og_stroke = strokes2[0]
        
        result_poly = bool_function(poly1, poly2)
        new_polys[f] = result_poly

    
    for f in new_polys.keys() :
        poly = new_polys[f]
        mapping = shapely.geometry.mapping(poly)

        delete_frame(layer1, f)
        new_frame = layer1.frames.new(f)

        if len(mapping["coordinates"]) > 0 :
            polygons_to_stroke(poly, og_stroke, new_frame, gp_obj, recycle_stroke=False, layer=layer1)

    for f in frame_list :
        bpy.context.scene.frame_set(f)
        boolean.refresh_geometry_display()


