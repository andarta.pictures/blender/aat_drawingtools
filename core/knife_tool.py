# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import numpy as np
import mathutils

from ..common import gp_utils
from ..common.geometry_3D import location_to_region, region_to_location, to_local

from . import boolean


def get_lasso_sections(geometry, lasso_path) :
    lasso_in_shape = boolean.is_contained_by(lasso_path, geometry)

    intersection_indices = []
    
    for i in range(1, len(lasso_in_shape)) :
        status_0 = lasso_in_shape[i-1]
        status_1 = lasso_in_shape[i]

        if status_0 != status_1 :
            intersection_indices.append(i)
    
    lasso_sections = []
    lasso_section_inside_status = []

    if len(intersection_indices) > 1 :
        for i in range(1, len(intersection_indices)) :
            status = lasso_in_shape[0] if i%2 == 0 else not lasso_in_shape[0]

            lasso_section_start = intersection_indices[i-1]-1
            lasso_section_end =  intersection_indices[i]+1
            lasso_section = lasso_path[lasso_section_start:lasso_section_end]

            lasso_sections.append(lasso_section)
            lasso_section_inside_status.append(status)
    
    return lasso_sections, lasso_section_inside_status


def get_rotation_step(geometry, full_lasso, lasso_sections) :

    if len(lasso_sections) == 0 :
        return
    elif len(lasso_sections) == 1 :
        lasso_section = lasso_sections[0]
        intersection_id1, intersection1 = segment_intersection(geometry, (lasso_section[0], lasso_section[1]))
        intersection_id2, intersection2 = segment_intersection(geometry, (lasso_section[-2], lasso_section[-1]))

        geometry_section_start = min(intersection_id1, intersection_id2)
        geometry_section_end = max(intersection_id1, intersection_id2)

        geometry_inside_lasso = boolean.is_contained_by(geometry, full_lasso).tolist()
        
        half1 = geometry_inside_lasso[geometry_section_start:geometry_section_end]
        h1 = np.nonzero(np.array(half1))[0].shape[0]
        
        half2 = geometry_inside_lasso[:geometry_section_start] + geometry_inside_lasso[geometry_section_end+1:]
        h2 = np.nonzero(np.array(half2))[0].shape[0]

        if intersection_id1 < intersection_id2 :
            step = 1
        if intersection_id1 > intersection_id2 :
            step = -1
        if h2 > h1 :
            step = -step
        
        print("STEP : ", step)

        return step
    
    else :
        intersections = []
        for lasso_section in lasso_sections :
            intersection_id1, intersection1 = segment_intersection(geometry, (lasso_section[0], lasso_section[1]))
            intersection_id2, intersection2 = segment_intersection(geometry, (lasso_section[-2], lasso_section[-1]))

            intersections.append((intersection_id1, intersection_id2))
        
        print(intersections)
        intersection_span = [abs(e[1]-e[0]) for e in intersections]
        print(intersection_span)
        print(np.argmin(intersection_span))

        smallest_section = intersections[np.argmin(intersection_span)]
        if smallest_section[0] < smallest_section[1] : 
            step = 1
        else :
            step = -1
        
        print("STEP : ", step)

        return step
    

def segment_intersection(points, segment) :
    """Numpy implementation of the algorithm described in this post : 
    https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect#answer-565282"""

    P_list = [np.array(points[i-1]) for i in range(1, len(points))]
    P = np.stack(P_list)

    R_list = [np.array(points[i])-np.array(points[i-1]) for i in range(1, len(points))]
    R = np.stack(R_list)

    q = np.array(segment[0])
    s = np.array(segment[1]) - q

    u = np.cross((q-P), R) / np.cross(R, s)
    t = np.cross((q-P), s) / np.cross(R, s)

    intersection_status = (0 <= u) & (u <= 1) & (0 <= t) & (t <= 1)
    intersection_indices = np.nonzero(intersection_status)[0]

    if len(intersection_indices) == 0 :
        return None, None

    i = intersection_indices[0]
    intersection = q+u[i]*s

    return i, intersection


def get_geometry_section(geometry, section_start, section_end, step) :
    indices = []
    i=section_start 
    indices.append(i)
    
    while i != section_end :
        i+=step
        
        if i >= len(geometry) :
            i = 0
        if i < 0 : 
            i = len(geometry)-1

        indices.append(i)
    
    geometry_section = [geometry[i] for i in indices]

    return geometry_section, indices

def get_cumulative_distance(point_section) :
    cumulative_distance = [0]
    p0 = point_section[0]
    total_distance = 0
    for p1 in point_section[1:] :
        dist = np.linalg.norm(np.array(p1)-np.array(p0))
        total_distance += dist
        cumulative_distance.append(total_distance)
        p0 = p1

    return cumulative_distance

def get_path_relative_indices(relative_distance_1, relative_distance_2) :
    indices = []
    for d in relative_distance_1 :
        i = 0
        while relative_distance_2[i] <= d and i<len(relative_distance_2)-1 :
            i+=1

        if relative_distance_2[i]-relative_distance_2[i-1] == 0 :
            ir = i-1
        else :
            ir = i-1+(d-relative_distance_2[i-1])/(relative_distance_2[i]-relative_distance_2[i-1])

        indices.append(ir)
    return indices

def indices_to_positions(indices, path) :
    new_positions = []

    for j in indices :
        i = int(j)
        r = j-i
        if r == 0 :
            new_positions.append(path[i])
        else :
            p1 = np.array(path[i])
            p2 = np.array(path[i+1])
            pos = p1+(p2-p1)*r
            new_positions.append(pos)
    
    return new_positions


def displace_geometry_along_path(geometry, path) :
    """
    Displaces a series of points to fit on a 2D path. For each point in the original 
    geometry, a new position will be calculated by placing it proportionately along 
    the path (if it was placed at 30% of the original segment length, it will be 
    placed at 30% of the path.) First point is always placed at beginning of path, 
    last point at the end.

    - geometry (2D tuple list) : list of 2D points to be displaced
    - path (2D tuple list) : list of 2D points to serve as a guideline to place points on
    
    """

    if len(geometry) < 2 or len(path) < 2 :
        return
    
    cumulative_points_distance = get_cumulative_distance(geometry)
    cumulative_path_distance = get_cumulative_distance(path)

    if cumulative_points_distance[-1] == 0 or cumulative_path_distance[-1] == 0 :
        return 

    relative_points_distance = np.array(cumulative_points_distance)/cumulative_points_distance[-1]
    relative_path_distance = np.array(cumulative_path_distance)/cumulative_path_distance[-1]

    indices = get_path_relative_indices(relative_points_distance, relative_path_distance)
    positions = indices_to_positions(indices, path)

    return positions


def apply_geometry_displacement(stroke, gp_object, displaced_geometry, geometry_section_indices) :
    """
    Displaces points in stroke to a new position. The position stored in the 
    i-th element of displaced geometry will be affected to the index 
    contained in the i-th element of geometry_section_indices.

    - stroke : GPencilStroke to be displaced
    - gp_object : object containing said stroke (to access world matrix)
    - displaced_geometry : list of new positions (2D tuples)
    - geometry_section_indices : corresponding index of each position in stroke.points array
    
    """
    region, r3d = boolean.get_view_region()
    depth_reference = gp_object.matrix_world @ stroke.points[0].co

    for i in range(0, len(displaced_geometry)) :
        new_2D_pos = displaced_geometry[i]
        point_index = geometry_section_indices[i]

        new_global_3D_pos = region_to_location(new_2D_pos, depth_reference, region, r3d)
        new_local_3D_pos = to_local(np.array(new_global_3D_pos), gp_object.matrix_world)

        stroke.points[point_index].co = mathutils.Vector(new_local_3D_pos)


def cut_stroke(stroke, gp_object, path, mode="REMOVE") :
    """
    Applies given path to displace points in a stroke, either by adding 
    matter (if mode is 'ADD') or by removing some (if mode is 'REMOVE')
    
    - stroke : GPencilStroke to displace
    - gp_object : object containing said stroke (to access world matrix)
    - path : list of 2D viewport coordinates of the lasso path
    - mode : either 'ADD' or 'REMOVE'
    
    """

    region, r3d = boolean.get_view_region()
    geometry = [location_to_region(gp_object.matrix_world @ p.co, region, r3d) for p in stroke.points]

    lasso_sections, lasso_section_inside_status = get_lasso_sections(geometry, path)

    step = get_rotation_step(geometry, path, lasso_sections)

    for i in range(0, len(lasso_sections)) :
        lasso_section = lasso_sections[i]
        lasso_section_is_inside = lasso_section_inside_status[i]

        if mode == "REMOVE" and not lasso_section_is_inside :
            continue
        elif mode == "ADD" and lasso_section_is_inside :
            continue

        print("Displacing section #%d of lasso"%i)

        geometry_section_start, intersection1 = segment_intersection(geometry, (lasso_section[0], lasso_section[1]))
        geometry_section_end, intersection2 = segment_intersection(geometry, (lasso_section[-2], lasso_section[-1]))

        lasso_section = [intersection1]+lasso_section[1:-1]+[intersection2]
        
        geometry_section, geometry_section_indices = get_geometry_section(geometry, geometry_section_start, geometry_section_end, step)

        displaced_geometry_section = displace_geometry_along_path(geometry_section, lasso_section)

        if displaced_geometry_section is not None :
            apply_geometry_displacement(stroke, gp_object, displaced_geometry_section, geometry_section_indices)
    

def knife_cut(path, mode) :
    """Applies given path to displace intersecting strokes, either by adding 
    matter (if mode is 'ADD') or by removing some (if mode is 'REMOVE')
    
    - path : list of 2D viewport coordinates of the lasso path
    - mode : either 'ADD' or 'REMOVE'"""

    gp_obj = gp_utils.get_active_gp_object()
    active_layer = boolean.get_active_layer(gp_obj)

    if active_layer is None :
        return

    for s in active_layer.active_frame.strokes:
        active_layer.active_frame.strokes.close(s)
        cut_stroke(s, gp_obj, path, mode)