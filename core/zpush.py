# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import blf
import gpu
from gpu_extras.batch import batch_for_shader
import numpy as np
from mathutils import Matrix, Vector
import itertools

from ..common import geometry_3D as geo3D, log_3D, timer, blender_ui

font_id = 1

displayed_objects = []

class TextBox :


    def __init__(self, text, coordinates, text_size, layout, box_dimensions=(-1,-1), selectable=True, linked_objects=[], type="LAYER") :
        self.text = text
        self.coordinates = coordinates
        self.text_size = text_size
        self.type = type
        self.layout = layout
        self.alert_suffix = ""
        self.set_box_dimensions(box_dimensions)

        self.selectable = selectable
        self.dragged = False
        self.selected = False
        self.visible = True

        self.linked_objects = linked_objects

        self.subscribe()
    
    def alert(self, alert_status) :
        self.alert_suffix = " ⚠" if alert_status else ""
        self.set_box_dimensions(self.master_dimensions)

    def set_box_dimensions(self, input_dims) :
        self.master_dimensions = input_dims

        dx, dy = input_dims
        w,h = self.get_display_size()
        m = self.text_size

        dx = w+m if dx == -1 else dx
        dy = h+m if dy == -1 else dy

        self.box_dimensions = (dx, dy)

    def get_linked_elements(self) :
        linked = []
        for e in displayed_objects :
            if e.type == "LAYER" and e.text in self.linked_objects :
                linked.append(e)
        return linked
    
    def update_selected_status(self) :
        if self.type in ["ARROW", "DEPTH"] :
            self.selected = False
            for e in self.get_linked_elements() :
                if e.selected :
                    self.selected = True
                    return

    def get_color(self) :

        self.update_selected_status()

        if "_OPTION" in self.type :
            return (76/255, 191/255, 230/255, 1.0)
        elif self.selected :
            return (235/255, 153/255, 71/255, 1.0)
        else :
            r,g,b,a = log_3D.get_color("BLUE")
            return (r, g, b, 1.0)    

    def subscribe(self) :
        global displayed_objects
        context = bpy.context
        args = (self, context)

        self.handler = bpy.types.SpaceView3D.draw_handler_add(self.draw, args, 'WINDOW', 'POST_PIXEL')
        displayed_objects.append(self)


    def get_display_size(self) :
        alert_w = len(self.alert_suffix)
        return (self.text_size*(len(self.text)+alert_w)*0.6, self.text_size)
    
    def set_coordinates(self, x, y) :
        self.coordinates = (x,y,0)

    def get_box_dimensions(self) :
        return self.box_dimensions
    

    def center(self) :
        x, y, z = self.coordinates
        s_x, s_y = self.get_display_size()
        return (x+s_x*0.5, y+0.3*s_y, z)


    def unsubscribe(self) :
        global displayed_objects
        bpy.types.SpaceView3D.draw_handler_remove(self.handler, 'WINDOW')
        displayed_objects = [e for e in displayed_objects if e.text != self.text]


    def get_box_polyline(self) :
        w, h = self.box_dimensions

        x,y,z = self.center()

        polyline = [
            [x-w/2, y-h/2, z],
            [x+w/2, y-h/2, z],
            [x+w/2, y+h/2, z],
            [x-w/2, y+h/2, z],
            [x-w/2, y-h/2, z],
        ]

        return polyline
    

    def is_colliding(self, position) :
        x,y,z = self.center()
        w,h = self.get_box_dimensions()
        px,py = position

        x_collision = x-w/2 <= px and px <= x+w/2
        y_collision = y-h/2 <= py and py <= y+h/2

        return x_collision and y_collision


    def draw_box(self):
        shader = gpu.shader.from_builtin('UNIFORM_COLOR')
        gpu.state.blend_set('ALPHA')
        gpu.state.line_width_set(1.0)

        batch = batch_for_shader(shader, 'LINE_STRIP', {"pos": self.get_box_polyline()})
        shader.uniform_float("color", self.get_color())

        batch.draw(shader)

        # restore opengl defaults
        gpu.state.line_width_set(1.0)
        gpu.state.blend_set('NONE')


    def draw_text(self):
        """Draw method"""
        global font_id

        x,y,z = self.coordinates
        blf.position(font_id, x, y, z)
        blf.size(font_id, self.text_size)

        color = self.get_color()
        blf.color(font_id, color[0], color[1], color[2], color[3])
        
        blf.draw(font_id, self.text)

        w = self.text_size*(len(self.text)-1.0)*0.6
        
        blf.position(font_id, x+w, y, z)
        color = (230/255, 86/255, 213/255, 1.0)
        blf.color(font_id, color[0], color[1], color[2], color[3])
        blf.size(font_id, int(self.text_size*1.5))
        blf.draw(font_id, self.alert_suffix)


    def draw(self, op, context) :
        correct_region = context.region == self.layout.region
        if self.visible and correct_region :
            self.draw_text()
            self.draw_box()



class Reorder_Layout : 
    

    def __init__(self, gp_objects, base_position, region, text_size=13) :
        self.gp_objects = [o.name for o in gp_objects]
        self.base_position = base_position
        self.text_size = text_size
        self.region = region

        self.autoscale = True

        self.display_elements = []
        self.update_rounded_status()
        self.update_display()

    def get_display_structure(self) :
        struct = {}

        for o_name in self.gp_objects :
            o = bpy.data.objects[o_name]
            depth = o.location.y
            
            if depth in struct.keys() :
                struct[depth].append(o.name)
            else :
                struct[depth] = [o.name]
        
        return struct
    
    def get_selected_elements(self) :
        selected = []
        for e in self.display_elements :
            if e.selected and e.type == "LAYER" : 
                selected.append(e)

        return selected
    
    def toggle_selection(self, element) : 
        element.selected = not element.selected
        
        # if element.selected :
        #     obj = bpy.data.objects[element.text]
        #     bpy.context.view_layer.objects.active = obj
        
        self.update_blender_selection()
        blender_ui.full_redraw()

    def update_blender_selection(self) :
        for obj in bpy.context.selected_objects:
            obj.select_set(False)

        selected_elements = self.get_selected_elements()
        # print("Updating blender selection : ", ", ".join([e.text for e in selected_elements]))

        for e in selected_elements :
            gp = bpy.data.objects[e.text]
            gp.select_set(True)

    def get_drop_location_status(self, position) :
        target_y = position[1]

        elements = [e for e in self.display_elements if e.selectable and not e.dragged]

        prev_y_max = None

        for i in range(0, len(elements)) :
            e = elements[i]
            y = e.center()[1]
            h = e.get_box_dimensions()[1]

            y_min = y-h/2
            y_max = y+h/2

            if i == 0 :
                if target_y < y_min :
                    return "", e.text
            
            if i == len(elements)-1 :
                if target_y > y_max :
                    return e.text, ""

            if y_min <= target_y and target_y <= y_max :
                return e.text
            
            if prev_y_max is not None :
                if prev_y_max < target_y and target_y < y_min :
                    return elements[i-1].text, e.text
            
            prev_y_max = y_max

    
    def zfight_check(self) :

        struct = self.get_display_structure()
        to_alert = []

        for depth in struct.keys() : 
            if len(struct[depth]) < 2 :
                continue

            gp_objects = [bpy.data.objects[o_name] for o_name in struct[depth]]

            bounds = get_object_bounds(gp_objects)
            gp_objects = [gp for gp in gp_objects if gp.name in bounds.keys()]

            for gp1, gp2 in itertools.combinations(gp_objects, 2) :
                if hitbox_collision(bounds[gp1.name], bounds[gp2.name]) :
                    to_alert += [gp1.name, gp2.name]

        for e in self.display_elements :
            if e.type == "LAYER" and e.text in to_alert :
                e.alert(True)
            else :
                e.alert(False)
    
    def update_rounded_status(self) :
        self.rounded = self.all_depth_rounded()

    def all_depth_rounded(self) :
        for o_name in self.gp_objects :
            gp = bpy.data.objects[o_name]
            diff = min(abs(0.001-gp.location.y%0.001), gp.location.y%0.001)
            # print("Testing if %s is rounded : "%o_name, diff < 10**-7)
            if diff > 10**-7 :
                return False
        return True


    def display_group(self, depth, object_names, position) :
        x,y = position
        height = 0

        for i in range(0, len(object_names)) :
            box = TextBox(object_names[i], (x,y+height,0), self.text_size, self, (-1, -1), type="LAYER")
            w,h = box.get_box_dimensions()
            self.display_elements.append(box)
            height += h
        
        box = TextBox("%.3f"%depth, (0,0,0), self.text_size, self, (-1, height), selectable=False, linked_objects=object_names, type="DEPTH")
        w,h = box.get_box_dimensions()
        self.display_elements.append(box)
        box.set_coordinates(x-w, y+height/2-self.text_size)

        uparrow = TextBox("▴", (0,0,0), self.text_size, self, (-1, height/2), selectable=False, linked_objects=object_names, type="ARROW")
        self.display_elements.append(uparrow)
        w2,h2 = uparrow.get_box_dimensions()
        uparrow.set_coordinates(x-w-w2, y+0.75*height-self.text_size)
        uparrow.visible = False

        downarrow = TextBox("▾", (0,0,0), self.text_size, self, (-1, height/2), selectable=False, linked_objects=object_names, type="ARROW")
        self.display_elements.append(downarrow)
        w2,h2 = downarrow.get_box_dimensions()
        downarrow.set_coordinates(x-w-w2, y+0.25*height-self.text_size)
        downarrow.visible = False

        return height
    
    def display_options(self, position) :
        x,y = position

        height = 0

        box = TextBox("Zfight check", (x,y+height,0), self.text_size, self, (-1, -1), selectable=False, type="ZFIGHT_CHECK_OPTION")
        self.display_elements.append(box)
        w,h = box.get_box_dimensions()
        height += h

        status = "ON" if self.autoscale else "OFF"
        box = TextBox("Auto rescale : "+status, (x,y+height,0), self.text_size, self, (-1, -1), selectable=False, type="AUTOSCALE_OPTION")
        self.display_elements.append(box)
        w,h = box.get_box_dimensions()
        height += h

        if not self.rounded :
            box = TextBox("Round values", (x,y+height,0), self.text_size, self, (-1, -1), selectable=False, type="ROUNDVALUES_OPTION")
            self.display_elements.append(box)
            w,h = box.get_box_dimensions()
            height += h

        return height


    def update_display(self) :

        selected_names = [e.text for e in self.get_selected_elements() if e.type == "LAYER"]
        alerted_names = [e.text for e in self.display_elements if len(e.alert_suffix) > 0]
        # print("Alerted elements during display update : ", len(alerted_names))

        if len(self.display_elements) > 0 :
            self.destroy_display_elements()

        x,y = self.base_position
        display_structure = self.get_display_structure()
        depth_list = list(display_structure.keys())
        depth_list.sort()
        depth_list = list(reversed(depth_list))

        current_height = 0
        group_distance = 20

        h = self.display_options((x,y+current_height))
        current_height += h+group_distance*2

        for d in depth_list :
            h = self.display_group(d, display_structure[d], (x  ,y+current_height))
            current_height += h+group_distance

        for e in self.display_elements :
            if e.text in selected_names :
                e.selected = True
            if e.text in alerted_names :
                e.alert(True)
        
        return current_height


    def detect_collision(self, position) :
        for e in self.display_elements : 
            if e.is_colliding(position) :
                return e
    

    def destroy_display_elements(self) :
        global displayed_objects
        displayed_objects = []

        for e in self.display_elements :
            e.unsubscribe()
        self.display_elements = []


    def element_dropped(self, element, position) :
        drop = self.get_drop_location_status(position)
        dropped_gp = bpy.data.objects[element.text]

        displacement = 0
        new_depth = 0.0

        print("Drop status : ", drop)

        if type(drop) is str : 
            target_gp = bpy.data.objects[drop]
            print("Setting %s depth to %.3f (%s)" % (element.text, target_gp.location.y, drop))
            new_depth = target_gp.location.y

        if type(drop) is tuple :
            
            if drop[0] == "" :
                print("Dropping above")
                gp = bpy.data.objects[drop[1]]
                new_depth = gp.location.y+0.01
            
            elif drop[1] == "" :
                gp = bpy.data.objects[drop[0]]

                new_depth = gp.location.y-0.01
                print("Dropping below")

            else :
                gp1 = bpy.data.objects[drop[0]]
                gp2 = bpy.data.objects[drop[1]]
                new_depth = (gp2.location.y+gp1.location.y)/2

        displacement = new_depth - dropped_gp.location.y
        zpush_gp(dropped_gp, new_depth, autoscale=self.autoscale)

        return displacement
    

    def mouse_react(self, position) :
        o_name = self.get_drop_location_status(position)

        if type(o_name) is str :
            for e in self.display_elements :
                if e.type == "ARROW" :
                    if o_name in e.linked_objects :
                        e.visible = True
                    else :
                        e.visible = False
        
        else : 
            for e in self.display_elements :
                if e.type == "ARROW" :
                    e.visible = False

    def displace_all(self, displacement) :

        self.base_position = tuple(np.array(self.base_position)+np.array(displacement))

        displacement = np.array((displacement[0], displacement[1], 0))
        for e in self.display_elements : 
            new_coord = np.array(e.coordinates)+np.array(displacement)
            e.coordinates = Vector(new_coord.tolist())


def hitbox_collision(hitbox1, hitbox2) :
    min1 = hitbox1[0][[0,2]]
    max1 = hitbox1[1][[0,2]]
    min2 = hitbox2[0][[0,2]]
    max2 = hitbox2[1][[0,2]]

    # print("Hitbox 1 : %s and %s"%(np.round(min1, 2).tolist(), np.round(max1, 2).tolist()))
    # print("Hitbox 2 : %s and %s"%(np.round(min2, 2).tolist(), np.round(max2, 2).tolist()))

    hit_x = any([
        min1[0] >= min2[0] and min1[0] <= max2[0],
        max1[0] >= min2[0] and max1[0] <= max2[0],
    ])

    hit_y = any([
        min1[1] >= min2[1] and min1[1] <= max2[1],
        max1[1] >= min2[1] and max1[1] <= max2[1],
    ])

    return hit_x and hit_y


def get_object_bounds(gp_objects) :
    bounds = {}

    for gp in gp_objects :
        matrix1 = Matrix(gp.matrix_world)
        world_mat = np.asarray(matrix1)
        mat = world_mat[:3, :3]
        loc = world_mat[:3, 3]

        points = get_points(gp)
        
        if len(points) > 0 :
            local_min = np.min(points, axis=0)
            local_max = np.max(points, axis=0)

            world_min = local_min @ mat.T + loc
            world_max = local_max @ mat.T + loc

            bounds[gp.name] = (world_min, world_max)

    return bounds


def get_points(gp_object) :
    points = []
    for layer in gp_object.data.layers :
        for stroke in layer.active_frame.strokes :
            points += [p.co for p in stroke.points]
    return np.array(points)

def calculate_points_cache(gp_objects) :
    points_cache = {}
    for gp in gp_objects :
        points_cache[gp.name] = get_points(gp)
    return points_cache

def find_childof_contraint_chain(object) :
    child_of_constraints = [c for c in object.constraints if c.type == "CHILD_OF"]
    constraint = child_of_constraints[0] if len(child_of_constraints)>0 else None
    constraint_inventory = []
    if constraint is not None :
        constraint_inventory = find_childof_contraint_chain(constraint.target)
        constraint_inventory.append(constraint.target.name)
    return constraint_inventory

def zpush_gp(gp_object, new_depth, autoscale=False) :
    contraint_hierarchy = find_childof_contraint_chain(gp_object)
    if not autoscale or len(contraint_hierarchy) > 0 :
        gp_object.location.y = new_depth
        return

    scene = bpy.context.scene
    camera = scene.camera

    matrix1 = Matrix(gp_object.matrix_world)
    world_mat = np.asarray(matrix1)
    mat = world_mat[:3, :3]
    loc = world_mat[:3, 3]

    old_depth = gp_object.location.y
    gp_object.location.y = new_depth
    
    dst_origin = np.array(gp_object.location)
    camera_location = np.array(camera.location)
    
    c0 = np.array([0,0,0]) @ mat.T + loc
    c0_t = geo3D.calculate_intersection(dst_origin, np.array([0,1,0]), camera_location, c0-camera_location)

    scale_ratio = (new_depth-camera_location[1])/(old_depth-camera_location[1])
    new_scale = np.array(gp_object.scale)*scale_ratio

    gp_object.scale = Vector(new_scale.tolist())
    gp_object.location = Vector(c0_t)



