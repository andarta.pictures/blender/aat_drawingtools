import bpy

from ..common import gp_utils

def test() :
    print("TEST !")

    view_layer = bpy.context.window.view_layer

    for o in bpy.data.objects :
        if type(o.data).__name__ == "GreasePencil" :
            active = not gp_utils.is_excluded_from_viewlayer(o, view_layer)
            print(f"{o.name} : {active}")