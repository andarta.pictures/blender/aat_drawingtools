# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import gpu
from gpu_extras.batch import batch_for_shader
import numpy as np

from ..common import gp_utils, mode_manager, log_3D, timer
from ..common.geometry_3D import region_to_location
from ..common.gp_utils import refresh_geometry_display

from . import knife_tool


class Lasso :

    def __init__(self, context) :
        """Creates handle at given viewport position."""
        
        self.mode = "REMOVE"
        self.drawing = False

        args = (self, context)

        self.handler = bpy.types.SpaceView3D.draw_handler_add(self.draw, args, 'WINDOW', 'POST_PIXEL')

        self.path = []
    
    def update_cursor(self) :
        context = bpy.context
        if self.mode == "ADD" :
            context.window.cursor_set("PAINT_BRUSH")
            
        elif self.mode == "REMOVE" :
            context.window.cursor_set("KNIFE")
    
    def set_mode(self, mode) :
        assert mode in ["REMOVE", "ADD"]
        self.mode = mode
        self.update_cursor()
    
    def get_mode(self) :
        return self.mode

    def get_path(self) :
        return self.path
    
    def get_color(self) :
        if self.mode == "REMOVE" :
            return (0.9, 0.45, 0.0, 0.7)
        else :
            return (0.5, 1.0, 0.4, 0.7)


    def add_position(self, mouse_position) :
        self.path.append(mouse_position)

    def delete(self) :
        bpy.types.SpaceView3D.draw_handler_remove(self.handler, 'WINDOW')

    def draw(self, op, context):
        """Draw method"""

        shader = gpu.shader.from_builtin('UNIFORM_COLOR')
        gpu.state.blend_set('ALPHA')
        gpu.state.line_width_set(2.0)

        batch = batch_for_shader(shader, 'LINE_STRIP', {"pos": self.path})
        shader.uniform_float("color", self.get_color())

        batch.draw(shader)

        # restore opengl defaults
        gpu.state.line_width_set(1.0)
        gpu.state.blend_set('NONE')
    
    def reset_path(self) :
        self.path = []

    def apply(self) :
        print("Applying boolean with mode : ", self.mode)
        try :
            knife_tool.knife_cut(self.path, self.mode)
            refresh_geometry_display()
        finally :
            self.path = []


def ray_tracing_numpy(x,y,poly):
    """Fast implementation of the Ray Casting algorithm for Point in 
    Polygon problem. To learn about how it works please refer to 
    https://en.wikipedia.org/wiki/Point_in_polygon#Ray_casting_algorithm.
    
    Params : 
    - x : 1-D numpy array containing the x position of the n points to be tested.
    - y : 1-D numpy array containing the y position of the n points to be tested.
    - poly : 2-D array (numpy or list) with the coordinates of the m points of the poly.
    
    Returns a list of n booleans corresponding to each point status (True = inside, False = outside)"""

    n = len(poly)
    inside = np.zeros(len(x),np.bool_)
    p2x = 0.0
    p2y = 0.0
    xints = 0.0
    p1x,p1y = poly[0]

    for i in range(n+1):
        p2x,p2y = poly[i % n]

        p_in_interest_zone = (y > min(p1y,p2y)) & (y <= max(p1y,p2y)) & (x <= max(p1x,p2x)) 
        idx = np.nonzero(p_in_interest_zone)[0]

        if p1y != p2y :

            xints = (y[idx]-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
            if p1x == p2x:
                inside[idx] = ~inside[idx]
            
            else:
                idxx = idx[x[idx] <= xints]
                inside[idxx] = ~inside[idxx]  

        p1x,p1y = p2x,p2y
    
    return inside    

def is_contained_by(subject_polyline, container_polyline) :
    subject_polyline = np.array(subject_polyline)
    container_polyline = np.array(container_polyline)
    return ray_tracing_numpy(subject_polyline[:,0], subject_polyline[:,1], container_polyline)


def is_colliding(subject_polyline, container_polyline, mode="FAST") :
    assert mode in ['FAST', 'PRECISE']
    
    t = timer.Timer("1.2.2.1 Hitbox calculation")
    subject_hitbox = get_hitbox(subject_polyline)
    container_hitbox = get_hitbox(container_polyline)
    t.stop()

    t = timer.Timer("1.2.2.2 Hitbox collision")
    collision = hitbox_collide(subject_hitbox, container_hitbox)
    t.stop()

    if mode == "FAST" :
        return collision
    else :
        t = timer.Timer("1.2.2.3 Precise collision")
        precise_collision = any(is_contained_by(subject_polyline, container_polyline))
        t.stop()
        return precise_collision


def display_path(path, size=2.0, depth_point=None, color="RANDOM", close=False) :
    region, r3d = get_view_region()

    if depth_point is None :
        depth_point = get_default_depth()
    
    if close :
        path.append(path[0])

    world_coords = [region_to_location(p, depth_point, region, r3d) for p in path]

    log_3D.line(world_coords, color_name=color, size=size)
    
def get_default_depth() :
    gp_obj = gp_utils.get_active_gp_object()
    layers = gp_obj.data.layers
    l = layers.active
    depth_point = gp_obj.matrix_world @ l.active_frame.strokes[0].points[0].co
    return depth_point
    
def display_point(point, size=5, depth_point=None, color="GREEN") :
    region, r3d = get_view_region()

    if depth_point is None :
        depth_point = get_default_depth()

    world_coord = region_to_location(point, depth_point, region, r3d)

    log_3D.point(world_coord, color_name=color, size=size)


def get_view_region() :
    region = bpy.context.region
    r3d = bpy.context.space_data.region_3d

    return region, r3d



def is_between(n, bounds) :
    return bounds[0] < n and n < bounds[1]

def hitbox_collide(hitbox1, hitbox2) :
    h1_min_x, h1_min_y, h1_max_x, h1_max_y = hitbox1
    h2_min_x, h2_min_y, h2_max_x, h2_max_y = hitbox2

    x_collision = False
    y_collision = False

    if is_between(h2_min_x, (h1_min_x, h1_max_x)) :
        x_collision = True

    if is_between(h2_max_x, (h1_min_x, h1_max_x)) :
        x_collision = True

    if is_between(h2_min_y, (h1_min_y, h1_max_y)) :
        y_collision = True

    if is_between(h2_max_y, (h1_min_y, h1_max_y)) :
        y_collision = True
    
    return x_collision and y_collision


def get_hitbox(shape) :
    shape = np.array(shape)

    all_x, all_y = shape[:, 0], shape[:, 1]
    
    min_x, min_y = np.min(all_x), np.min(all_y)
    max_x, max_y = np.max(all_x), np.max(all_y)

    return min_x, min_y, max_x, max_y

def get_active_layer(gp_object) :
    layers = gp_object.data.layers
    l = layers.active
    
    if l is None :
        print("No active layer found.")
        return
    
    elif l.lock or l.hide or not l.active_frame :
        print("Active layer found, but was not suitable for editing (%s)"%("locked" if l.lock else "hidden" if l.hide else "not active" if not l.active_frame else "?"))
        return 
    
    return l




