# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.


import bpy
from bpy.props import StringProperty, BoolProperty
from .common.import_utils import ensure_lib


bl_info = {
    "name" : "AAT_DrawingTools",
    "author" : "Andarta",
    "description" : "",
    "blender" : (3, 6, 0),
    "version" : (0, 25),
    "location" : "",
    "warning" : "",
    "category" : "Generic"
}


optional_lib= [
    "shapely",
    "scipy",
    "pyclipper"
    ]

for lib in optional_lib :
    has_lib = ensure_lib(lib)
    if not has_lib:
        print("Warning: Could not import nor install " + lib + '\n'+
              'Some features may not work properly'+              
             '\n Try again and if this error persist install dependencies manually')
    
class Andarta_Drawing_Tools_AddonPref(bpy.types.AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __name__
    preserve_topo : BoolProperty(name="Preserve number of points for interpolation", default=False)

    def draw(self, context):
        layout = self.layout
        # row = layout.row()

        # row.prop(self, "ffmpeg_path")

from . import (
    ops,
    ui,
)

modules = [
    ops, 
    ui, 
]

def register():
    bpy.types.Scene.nijigp_working_plane = bpy.props.EnumProperty(
                        name='Working Plane',
                        items=[('X-Z', 'Front (X-Z)', ''),
                                ('Y-Z', 'Side (Y-Z)', ''),
                                ('X-Y', 'Top (X-Y)', ''),
                                ('VIEW', 'View', 'Use the current view as the 2D working plane'),
                                ('AUTO', 'Auto', 'Calculate the 2D plane automatically based on input points and view angle')],
                        default='AUTO',
                        description='The 2D (local) plane that most add-on operators are working on'
                        )
    bpy.types.Scene.nijigp_working_plane_layer_transform = bpy.props.BoolProperty(
                        default=True, 
                        description="Taking the active layer's transform into consideration when calculating the view angle"
                        )
    
    bpy.utils.register_class(Andarta_Drawing_Tools_AddonPref)


    for m in modules :
        m.register()

def unregister():
    for m in modules :
        m.unregister()
    bpy.utils.unregister_class(Andarta_Drawing_Tools_AddonPref)
