# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy

class ExtraDrawingToolsMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_extra_drawing_tools_menu"
    bl_label = "Extra drawing tools menu"

    def draw(self, context):
        row = self.layout.row()
        row.operator("gpencil.remove_vertex_paint", text="Remove all vertex paint")
        row = self.layout.row()
        row.operator("gpencil.sculpt_quick_select", text="Quick select")
        row = self.layout.row()
        row.prop(context.scene, "quick_select_keep_overlay", icon="MODIFIER", text="Keep overlays in Quick Select")
        row = self.layout.row()
        row.operator("anim.ewi_scene_cleanup", text="[EWI] Clean-up scene", icon="SHADERFX")
        # row = self.layout.row()
        # row.operator("gpencil.drawing_tools_test", icon = "NONE", text="💀 Spooky scary button 💀")



class Tools_Panel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "Drawing Tools"
    bl_idname = "OBJECT_PT_toolspanel"

    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"


    def draw(self, context):

        addon_prefs = context.preferences.addons[__package__].preferences
        
        row = self.layout.row(align=True)
        row.operator("anim.quick_edit", icon = "SURFACE_NCIRCLE", text="Quick Edit")
        row.operator("anim.lattice_deform", icon = "OUTLINER_DATA_LATTICE", text="Lattice Deform")
        
        row = self.layout.row(align=True)
        row.operator("anim.gp_potato_tool", icon = "SCULPTMODE_HLT", text="Knife Paint")
        row.prop(context.scene, "restrict_bool_to_active_material", icon="RESTRICT_COLOR_ON", text="")
        row.prop(addon_prefs, "preserve_topo", icon="MOD_INSTANCE", text="")
        
        row = self.layout.row(align=True)
        row.operator("anim.layer_union", icon = "SELECT_EXTEND", text="Union")
        row.operator("anim.layer_intersection", icon = "SELECT_INTERSECT", text="Inter")
        row.operator("anim.layer_difference", icon = "SELECT_SUBTRACT", text="Diff")
        row.prop(bpy.context.scene, "boolean_multiframe", icon="OUTLINER_OB_CURVES", emboss=True, text="")
        
        row = self.layout.row(align=True)
        row.operator("anim.perspective_grid_edit", icon = "VIEW_PERSPECTIVE", text="Perspective Grid")
        row.operator("anim.perspective_grid_toggle", icon = "HIDE_OFF", text="")
        row.operator("anim.create_vanishing_point", icon = "PIVOT_CURSOR", text="")
        
        row = self.layout.row()
        row.operator("anim.gp_reorder", icon = "SEQ_SEQUENCER", text="Reorder elements")
        
        row = self.layout.row()
        row.operator("gpencil.aat_smart_fill",icon = "BRUSH_TEXFILL", text="Smart Fill")
        row.operator("gpencil.open_extra_tools_menu", icon="TRIA_DOWN", text="")
        

def register() :
    bpy.utils.register_class(Tools_Panel)
    bpy.utils.register_class(ExtraDrawingToolsMenu)
    bpy.types.Scene.boolean_multiframe = bpy.props.BoolProperty(name="Boolean multiframe mode", default=False)

def unregister() :
    del bpy.types.Scene.boolean_multiframe
    bpy.utils.unregister_class(ExtraDrawingToolsMenu)
    bpy.utils.unregister_class(Tools_Panel)
