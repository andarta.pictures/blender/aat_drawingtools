import bpy
from typing import Optional
from mathutils import Vector, Matrix
import numpy as np

from . import mode_manager, geometry_3D

def get_active_gp_object() -> Optional[bpy.types.Object]:
    """Get the active grease pencil object if any, None otherwise."""
    active_obj = bpy.context.active_object
    if not active_obj or not isinstance(active_obj.data, bpy.types.GreasePencil):
        return None
    return active_obj

def get_all_gp_objects() :
    """Returns a list of all Grease Pencil objects."""

    gp_objects = []

    for o in bpy.data.objects :
        if isinstance(o.data, bpy.types.GreasePencil) :
            gp_objects.append(o)

    return gp_objects 

def get_scene_gp_objects() :
    """Returns a list of all Grease Pencil objects."""
    
    scene = bpy.context.scene
    gp_objects = []

    for o in scene.objects :
        if isinstance(o.data, bpy.types.GreasePencil) :
            gp_objects.append(o)

    return gp_objects 

def select_all_gp_geometry(gp_obj, multiedit=False) :
    """Select every stroke for every layer in a Grease Pencil object.
    
    - gp_obj : Grease Pencil to select
    - multiedit : if True, also selects every stroke for every frame selected, if False, only on active frame"""

    layers = gp_obj.data.layers
    
    for l in layers :
        if l.lock or l.hide or not l.active_frame :
            continue
        
        if multiedit:
            target_frames = [f for f in l.frames if f.select]
        else:
            target_frames = [l.active_frame]

        for f in target_frames:
            for s in f.strokes:
                s.select = True

def get_active_gp_layer_framelist() :
    """Returns a list containing the time position of every frame in the Grease Pencil"""
    active_gp = get_active_gp_object()
    frame_n = []

    if active_gp is not None :
        layers = active_gp.data.layers
        for f in layers.active.frames :
            frame_n.append(f.frame_number)
        frame_n.sort()
    
    return frame_n

def get_object_depth(gp_object) :
    """Returns Y dimension of the mean point position of a given gp_object.
    
    WARNING : can be slow on high amount of points"""

    geo = get_geo(gp_object)
    geo = np.array(geo)
    centroid = np.mean(geo, axis=0)
    return float(centroid[1])

def sort_by_depth(gp_object_list) :
    """Sorts given gp_object_list by depth using get_object_depth()
    
    WARNING : can be slow on high amount of points"""

    depth = [get_object_depth(o) for o in gp_object_list]
    order = np.array(depth).argsort()
    return [gp_object_list[i] for i in reversed(order)]

def get_geo(obj, stroke_length_threshold=None, current_frame_only=False) :
    """Returns a list of points coordinates in the geometry of selected GP elements."""
    coords = []
    layers = obj.data.layers

    for l in layers :
        frames = [l.active_frame] if current_frame_only else l.frames
        for f in frames:
            for s in f.strokes:

                if stroke_length_threshold is not None :
                    assert isinstance(stroke_length_threshold, int), "stroke_length_threshold should be either None or an int"
                    if len(s.points) < stroke_length_threshold :
                        continue
                        
                for p in s.points:
                    coords.append(obj.matrix_world @ p.co)

    if len(coords) == 0 :
        coords.append(obj.matrix_world @ Vector((0,0,0)))

    return coords

def get_collection_list() :
    """Returns a list of every collection in the project."""
    scene = bpy.context.scene
    collecs = [c for c in bpy.data.collections] + [scene.collection]
    return collecs

def get_parent_collection_list(obj) :
    collection_list = []
    
    for c in bpy.data.collections :
        if obj.name in [o.name for o in c.all_objects] :
            collection_list.append(c)
    
    return collection_list
        

def get_layer_collection(collection, view_layer=None):
    '''Returns the view layer LayerCollection for a specificied Collection'''

    if view_layer is None:
        view_layer = bpy.context.view_layer

    candidates = recursive_children(view_layer.layer_collection)
    # print(f"Searching {collection.name} in {[lc.collection.name for lc in candidates]}")
    
    pop = lambda l : l[0] if len(l) > 0 else None
    return pop([lc for lc in candidates if lc.collection == collection])

def is_collection_hierarchy_active(obj, viewlayer=None) :
    excluded = []

    if viewlayer is None :
        viewlayer = bpy.context.window.view_layer

    for col in get_parent_collection_list(obj) : 
        layer_collection = get_layer_collection(col, viewlayer)
        
        if layer_collection is None :
            excluded.append(True)
        
        else :
            excluded.append(layer_collection.exclude)

    return True not in excluded


def refresh_geometry_display(object=None) :
    og_obj = bpy.context.view_layer.objects.active

    mode = mode_manager.switch_mode('OBJECT')
    if object is not None :
        bpy.context.view_layer.objects.active = object

    mode_manager.switch_mode('EDIT_GPENCIL')

    bpy.ops.gpencil.select_all(action='SELECT')
    bpy.ops.transform.translate(value=(.0, .0, .0), 
                                orient_type='GLOBAL', 
                                orient_matrix_type='GLOBAL', mirror=False, 
                                )
    
    mode_manager.switch_mode('OBJECT')
    bpy.context.view_layer.objects.active = og_obj

    mode.switch()

def safe_scale(scale) : 
    # Replace 0.0 scale value by a value close to zero. 
    # Trades precision for invertibility of world_matrix.
    new_scale = []
    for c in scale : 
        if c == 0 : 
            c = 0.00001
        new_scale.append(c)

    return Vector(new_scale)

def get_layer_matrix(layer, use_safe_scale=True) :
    # Helps avoid running into invertibility problems on the composed matrix.
    scale = safe_scale(layer.scale) if use_safe_scale else layer.scale

    # Convert the individual loc/rot/scale into a 4x4 matrix.
    layer_matrix = geometry_3D.compose_matrix(layer.location, scale, layer.rotation)
    return Matrix(layer_matrix.tolist())


def stroke_material(stroke, gp_obj) :
    mat_idx = stroke.material_index
    return gp_obj.material_slots[mat_idx].material


def remove_vertex_paint(gp_obj) :
    for layer in gp_obj.data.layers :
        for frame in layer.frames :
            for stroke in frame.strokes :
                stroke.vertex_color_fill = (0,0,0,0)
                for p in stroke.points :
                    p.vertex_color = (0,0,0,0)

def is_excluded_from_viewlayer(obj, view_layer=None) :
    if view_layer is None :
        view_layer = bpy.context.window.view_layer

    parent_layer_collections = []
    for lc in recursive_children(view_layer.layer_collection) :
        if obj.name in [o.name for o in lc.collection.all_objects] :
            parent_layer_collections.append(lc)

    # print(f"Parent collections of '{obj.name}' : {[lc.collection.name for lc in parent_layer_collections]}")
    return True in [lc.exclude for lc in parent_layer_collections]

def recursive_children(t):
    """Get all children recursively.
    From https://blender.stackexchange.com/a/167889/4979
    """

    yield t
    for child in t.children:
        yield from recursive_children(child)

        
def set_use_light(obj:bpy.types.Object, status:bool) :
    """Ensures the use_light option is turned on or off in the gp 
    object and all its layers."""

    # Set option in object
    obj.use_grease_pencil_lights = status
    
    # Set option in layers
    for l in obj.data.layers :
        l.use_lights = status