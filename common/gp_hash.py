import hashlib
import array

from ..common import timer

pop = lambda l : l[0] if len(l) > 0 else None

def gp_data_length(gp) :
    n = 0
    for l in gp.data.layers :
        for f in l.frames : 
            for s in f.strokes : 
                n += len(s.points)
    return n


def get_active_frame(frames, frame_n) :
    before_frames = [f for f in frames if f.frame_number <= frame_n]
    before_frames.sort(key=lambda f : f.frame_number)
    return before_frames[-1] if len(before_frames) > 0 else None

def pick_points(gp, pickrate, frame_n=None) : 
    picked = []

    size = gp_data_length(gp)
    step = max(1,int(size*pickrate))

    for l in gp.data.layers :
        frames = l.frames

        # Restrict point collection to frame_n if specified
        if frame_n is not None : 
            frames = [get_active_frame(frames, frame_n)]
            frames = [f for f in frames if f is not None]

        # Collect one every *step* points in each stroke of each frames
        for f in frames : 
            for s in f.strokes : 
                stroke_length = len(s.points)
                j = 0
                while j < stroke_length :
                    picked += list(s.points[j].co)
                    j += step
    
    return picked


def get_animation_data(gp) :
    anim_data = []

    if gp.animation_data is not None and gp.animation_data.action is not None:
        for fcurve in gp.animation_data.action.fcurves:
            if fcurve.data_path in ["location", "rotation_euler", "scale"] :
                keyframes = [(kf.co.x, kf.co.y) for kf in fcurve.keyframe_points]
                anim_data.append(("%s[%d]" % (fcurve.data_path, fcurve.array_index), keyframes))
    
    anim_data.sort(key=lambda e : e[0])

    return anim_data


def assert_validity(obj) : 
    if type(obj).__name__ != "Object" :
        raise ValueError(f"Provided object {str(obj)} is not a Blender object ({type(obj).__name__})")



### PUBLIC ###


def hash(gp, pickrate=0.02, hex=False, tolerate_objects=True) :
    assert_validity(gp)
    
    digest = lambda h : h.hexdigest() if hex else h.digest()
    h = hashlib.sha256()

    anim_data = str(get_animation_data(gp))
    h.update(anim_data.encode())

    if type(gp.data).__name__ != "GreasePencil" :
        if tolerate_objects :
            return digest(h)
        else :
            raise ValueError(f"Provided object doesn't have a legal type for hashing ({type(gp.data).__name__})")

    points = pick_points(gp, pickrate)

    points_hashable = array.array('f', points)
    h.update(points_hashable)

    visible_layers = [(l.info, l.opacity) for l in gp.data.layers if not l.hide]
    h.update(str(visible_layers).encode())

    return digest(h)

def frame_state(object, frame_n, pickrate=0.02) : 
    """Returns a dict reflecting the state of a grease pencil at a specific frame. 
    Contains object transform, any animated properties current value, and a hash 
    reflection the state of the points in the gp geometry (using pick_rate to 
    sample only a portion of all the existing points.)"""

    state = {}

    # Object transform
    state['matrix_world'] = str(object.matrix_world)

    def extract_fcurves_data(anim_data) : 
        blacklist = ["GPLYR_mask_"]
        is_blacklisted = lambda data_id : True in [pattern in data_id for pattern in blacklist]

        state = {}
        if anim_data and anim_data.action:
            for fcurve in anim_data.action.fcurves:
                data_id = "%s.%d" % (fcurve.data_path, fcurve.array_index)
                if not is_blacklisted(data_id) :
                    state[data_id] = "%.5f" % fcurve.evaluate(frame_n)
        return state
    
    # Extract any animated value from both the object and its data
    state.update(extract_fcurves_data(object.animation_data))
    state.update(extract_fcurves_data(object.data.animation_data))

    if type(object.data).__name__ == "GreasePencil" :

        # Gather a specified portion of all the points in the greasepencil geometry
        points = pick_points(object, pickrate, frame_n=frame_n)

        # Turn them into a hash for better readability
        h = hashlib.sha256()
        points_hashable = array.array('f', points)
        h.update(points_hashable)
        state["points"] = h.hexdigest()
    
    return state


def compare_frames(objects, frame1, frame2, pickrate=0.02) : 
    """Returns a list of all the objects properties that were changed between 
    the two specified frames. Usually used for debug purposes to get more 
    detailed informations when the hash compare is not accurate."""

    # List every object that has changed between the two frames
    changed_objects = []
    for obj in objects : 
        hash1 = hash_frame([obj], frame1, pickrate, hex=True)
        hash2 = hash_frame([obj], frame2, pickrate, hex=True)
        if hash1 != hash2 : 
            changed_objects.append(obj)
    
    # Then produce a detailed list of changed properties in changed objects
    changed_properties = []
    for obj in changed_objects : 
        state1 = frame_state(obj, frame1)
        state2 = frame_state(obj, frame2)

        for k in state1.keys() :
            if state1[k] != state2[k] : 
                changed_properties.append("%s > %s" % (obj.name, k))

    return changed_properties


def hash_frame(object_list, frame_n, pickrate=0.02, hex=False) -> str : 
    """Returns a hash of a specific frame.
    
    Args : 
        object_list : list of objects to be analyzed
        frame_n : frame to analyze
        pickrate : ratio of points to be analyzed (1.0=all, 0.02=2%)
        hex : format of the hash to be returns, between decimal and hexadecimal

    """

    h = hashlib.sha256()
    digest = lambda h : h.hexdigest() if hex else h.digest()

    for o in object_list :
        assert_validity(o)
        state = frame_state(o, frame_n, pickrate)
        state_keys = sorted(list(state.keys()))
        for k in state_keys : 
            h.update(state[k].encode())

    return digest(h)
