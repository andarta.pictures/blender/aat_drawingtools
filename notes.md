0.3.20:
    DRAWING TOOLS
            SMART FILL
            DONE add possibility to select multiple strokes and treat them as one 
            DONE make 'CLEAR' option more obvious
            SOLVED BUG scypy installation blocking failure. Was trying to install submodule instead of library


0.3.18:
    DRAWING TOOLS
        SMART FILL
            DONE implement smart fill option from NijiGPEN
            DONE add a force install for external library. Throw a non blocking error message in blender if it fails
            DONE add multiple hint layer selection
            DONE add default name GPLYR_FILL and GPLYR_STROKE 
            DONE auto unlock fill on launch
