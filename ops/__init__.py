# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import numpy as np

from ..common.utils import register_classes, unregister_classes
from ..common.keymaps import register_keymap

from .booleans import AAT_OT_gp_potato_tool, AAT_OT_layer_boolean, AAT_OT_layer_intersection, AAT_OT_layer_union, AAT_OT_layer_difference
from .transforms import AAT_OT_lattice_deform, AAT_OT_quick_edit
from .perspective_grid import AAT_OT_perspective_grid_edit, AAT_OT_perspective_grid_toggle, AAT_OT_create_vanishing_point
from .reorder import GPEDIT_OT_edit_gp_depth, GPEDIT_OT_gp_reorder
from .smart_fill import AAT_OT_SmartFillOperator
from .misc import AAT_OT_remove_vertex_paint, AAT_OT_open_extra_tools_menu, AAT_OT_gp_attenuate_tool, AAT_OT_cull_weak_strokes, AAT_OT_drawingtools_test, AAT_OT_quick_select, AAT_OT_clear_frame, AAT_OT_QuickTransform, AAT_OT_ewi_scene_cleanup


classes = [
    AAT_OT_quick_edit, 
    AAT_OT_lattice_deform,
    
    AAT_OT_perspective_grid_edit, 
    AAT_OT_perspective_grid_toggle, 
    AAT_OT_create_vanishing_point,
    
    AAT_OT_gp_potato_tool,
    AAT_OT_layer_boolean,
    AAT_OT_layer_union,
    AAT_OT_layer_intersection,
    AAT_OT_layer_difference,

    GPEDIT_OT_gp_reorder,
    GPEDIT_OT_edit_gp_depth,

    AAT_OT_SmartFillOperator,

    AAT_OT_remove_vertex_paint,
    AAT_OT_gp_attenuate_tool,
    AAT_OT_cull_weak_strokes,
    AAT_OT_quick_select,
    AAT_OT_open_extra_tools_menu,
    AAT_OT_drawingtools_test,
    AAT_OT_clear_frame,
    AAT_OT_QuickTransform,
    AAT_OT_ewi_scene_cleanup,
]

def register() :
    register_classes(classes)
    register_keymap("anim.quick_edit", "T", ctrl=True)
    register_keymap("anim.lattice_deform", "T", ctrl=True, alt=True)
    register_keymap("anim.gp_reorder", "R", shift=True, value='PRESS', space_type="VIEW_3D")
    # register_keymap("anim.lattice_deform", "B", category_name="Grease Pencil (Global)")
    register_keymap("anim.gp_potato_tool", "K", space_type="VIEW_3D")
    bpy.types.Scene.restrict_bool_to_active_material = bpy.props.BoolProperty(default=False, name="Restrict knife paint tool to active material")
    register_keymap("gpencil.clear_frame", "X", alt=True)
    register_keymap("gpencil.sculpt_quick_select", "W", alt=True)
    bpy.types.Scene.quick_select_keep_overlay = bpy.props.BoolProperty(default=False, name="Display overlay during Quick Select")

def unregister() :
    del bpy.types.Scene.restrict_bool_to_active_material
    del bpy.types.Scene.quick_select_keep_overlay
    unregister_classes(classes)