import bpy
from mathutils import Vector

from ..core.UI_elements import PerspectiveGrid
from ..common.geometry_3D import region_to_location



pers_grid = None

class AAT_OT_perspective_grid_edit(bpy.types.Operator) :
    bl_idname = "anim.perspective_grid_edit"
    bl_label = "Perspective grid in Grease Pencil"
    bl_options = {'REGISTER', 'UNDO'}

    def modal(self, context, event):
        context.area.tag_redraw()

        self.pers_grid.on_modal(context, event)
        
        if event.type in ["RET", "NUMPAD_ENTER"] and event.value == "PRESS" :
            self.pers_grid.set_active(not self.pers_grid.active)
            return {"FINISHED"}
        
        if event.type == "ESC" and event.value == "PRESS" :
            self.pers_grid.set_visible(False)
            return {"FINISHED"}

        if not self.pers_grid.active :
            return {'PASS_THROUGH'}
        
        if event.type == "NUMPAD_PLUS" and event.value == "PRESS" :
            n = self.pers_grid.get_ray_n()
            self.pers_grid.set_ray_n(n+1)
            return {"RUNNING_MODAL"}
        
        if event.type == "NUMPAD_MINUS" and event.value == "PRESS" :
            n = self.pers_grid.get_ray_n()
            if n > 1 :
                self.pers_grid.set_ray_n(n-1)
            return {"RUNNING_MODAL"}
        
        if event.type == "PAGE_UP" and event.value == "PRESS" :
            opacity = self.pers_grid.get_opacity()
            self.pers_grid.set_opacity(opacity+0.1)
            return {"RUNNING_MODAL"}
        
        if event.type == "PAGE_DOWN" and event.value == "PRESS" :
            opacity = self.pers_grid.get_opacity()
            self.pers_grid.set_opacity(opacity-0.1)
            return {"RUNNING_MODAL"}

        if event.type == "LEFTMOUSE" and event.value == "PRESS" :
            return {"RUNNING_MODAL"}
        
        if event.type == "LEFT_SHIFT" or event.type == "RIGHT_SHIFT" :
            if event.value == "PRESS" :
                self.pers_grid.keep_horizontal = True
            elif event.value == "RELEASE" :
                self.pers_grid.keep_horizontal = False
    
        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        global pers_grid

        print("Edit pers grid launched")

        if pers_grid is None :
            print("Creating new grid")
            pers_grid = PerspectiveGrid(context)
        else :
            print("Reusing existing grid.")
        
        self.pers_grid = pers_grid
        self.pers_grid.set_visible(True)
        self.pers_grid.set_active(True)            

        context.window_manager.modal_handler_add(self)

        return {'RUNNING_MODAL'}

class AAT_OT_perspective_grid_toggle(bpy.types.Operator) :
    bl_idname = "anim.perspective_grid_toggle"
    bl_label = "Perspective grid in Grease Pencil"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        global pers_grid

        print("Edit pers grid launched")

        if pers_grid is None :
            return {"CANCELLED"}
        
        pers_grid.set_visible(not pers_grid.visible)

        return {'FINISHED'}

class AAT_OT_create_vanishing_point(bpy.types.Operator) :
    """Click 4 times on screen to create a vanishing point. The first two clicks 
    describe the first vanishing line, the last two the second line."""
    
    bl_idname = "anim.create_vanishing_point"
    bl_label = "Add vanishing point"
    bl_options = {'REGISTER', 'UNDO'}

    def calculate_intersection(self) :
        p0, p1, p2, p3 = self.points

        a1 = (p0[1]-p1[1])/(p0[0]-p1[0])
        b1 = p0[1]-a1*p0[0]

        a2 = (p2[1]-p3[1])/(p2[0]-p3[0])
        b2 = p2[1]-a2*p2[0]

        x = (b2-b1)/(a1-a2)
        y = a1*x+b1

        return (x,y)
    
    def create_point(self) :
        empty = bpy.data.objects.new("VANISHING_POINT", None)
        empty.empty_display_type = 'PLAIN_AXES'

        pos_2D = self.calculate_intersection()
        pos_3D = region_to_location(Vector(pos_2D), bpy.context.active_object.location)
        print("Creating empty at : ", pos_3D)

        empty.location = pos_3D
        bpy.context.scene.collection.objects.link(empty)
        pass

    def modal(self, context, event):
        if len(self.points) >= 4 :
            print("Points acquired : ", self.points)
            self.create_point()
            return {"FINISHED"}

        if event.type == "RIGHTMOUSE" and event.value == "PRESS" :
            return {"FINISHED"}

        if event.type == "LEFTMOUSE" and event.value == "PRESS" :
            mouse_position = (event.mouse_region_x, event.mouse_region_y)
            self.points.append(mouse_position)
            print("Added point : ", mouse_position)
            return {"RUNNING_MODAL"}
    
        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        self.points = []
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}