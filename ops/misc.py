import bpy
import mathutils
import numpy as np
import math

from .booleans import base_lasso_init, base_lasso_modal

from ..core import boolean, dynamic_boolean, misc
from ..core.lattice_control import LatticeControl

from ..common import gp_utils, blender_ui, geometry_3D as geo3D, timer, mode_manager


class AAT_OT_remove_vertex_paint(bpy.types.Operator) :
    bl_idname = "gpencil.remove_vertex_paint"
    bl_label = "Remove vertex paint"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        for o in bpy.context.scene.objects :
            if o.type == 'GPENCIL' and not o.library and o.select_get() :
                gp_utils.remove_vertex_paint(o)

        return {'FINISHED'}
    

class AAT_OT_open_extra_tools_menu(bpy.types.Operator) :
    bl_idname = "gpencil.open_extra_tools_menu"
    bl_label = "Open extra tools menu"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.wm.call_menu(name="OBJECT_MT_extra_drawing_tools_menu")
        return {'FINISHED'}
    

class AAT_OT_drawingtools_test(bpy.types.Operator) :
    """Warning : clicking might result in various bugs, or in some cases an extremely painful death."""
    bl_idname = "gpencil.drawing_tools_test"
    bl_label = "Test operator"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        misc.test()
        return {'FINISHED'}
    


class AAT_OT_gp_attenuate_tool(bpy.types.Operator) :
    bl_idname = "anim.gp_attenuate_tool"
    bl_label = "Attenuate"
    bl_options = {'REGISTER', 'UNDO'}

    lasso_UI : boolean.Lasso

    def apply_cut(self) :
        try :
            dynamic_boolean.apply_attenuate(self.lasso_UI.get_path(), self.lasso_UI.get_mode())
            boolean.refresh_geometry_display()
        
        finally :
            self.lasso_UI.reset_path()
            
        self.lasso_UI.drawing = False
        bpy.ops.ed.undo_push(message = "Attenuate")


    def modal(self, context, event):
        if event.type == "LEFT_CTRL" or event.type == "RIGHT_CTRL" :

            if event.value == "PRESS" :
                self.lasso_UI.set_mode("REMOVE")
            
            if event.value == "RELEASE" :
                self.lasso_UI.set_mode("ADD")

        status = base_lasso_modal(context, event, self.lasso_UI, self.apply_cut)

        if 'FINISHED' in status :
            pop = lambda l : l[0] if len(l) > 0 else None
            current_tool = pop([e for e in bpy.context.workspace.tools if e.mode == bpy.context.object.mode])
            if current_tool is not None :
                print("Refreshing tool : ", current_tool.idname)
                bpy.ops.wm.tool_set_by_id(name=current_tool.idname)

            print("Ending attenuate mode.")

        return status


    def invoke(self, context, event):
        print("Running attenuate modal.")
        self.lasso_UI = base_lasso_init(context, event)
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}


def cull(frame, value) :
    to_delete = []
    for stroke in frame.strokes :
        strength_values = [p.strength for p in stroke.points]
        max_strength = max(strength_values) if len(strength_values) > 0 else 0.0
        if max_strength < value :
            to_delete.append(stroke)
    
    for s in to_delete :
        frame.strokes.remove(s)
    
    return len(to_delete)


class AAT_OT_cull_weak_strokes(bpy.types.Operator) :
    bl_idname = "gpencil.cull_weak_strokes"
    bl_label = "Cull weak strokes"
    bl_options = {'REGISTER', 'UNDO'}

    threshold : bpy.props.FloatProperty(default=0.03, step=0.5, min=0.0, max=1.0)

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self, width = 150)

    def draw(self, context):
        layout = self.layout
        layout.prop(self, 'threshold', text = '')

    def execute(self, context):
        for o in bpy.context.selected_objects :
            if o.type == 'GPENCIL' and not o.library :
                for layer in o.data.layers :
                    for frame in layer.frames :
                        cull(frame, self.threshold)

        return {'FINISHED'}


def set_overlay_visible(visible) :
    for area in bpy.context.screen.areas:
        if area.type == 'VIEW_3D':
            space = area.spaces.active
            if space.type == 'VIEW_3D':
                space.overlay.show_overlays = visible

class AAT_OT_quick_select(bpy.types.Operator) :
    bl_idname = "gpencil.sculpt_quick_select"
    bl_label = "Quick Select"
    bl_options = {'REGISTER', 'UNDO'}

    previous_tool_id = bpy.props.StringProperty(default="")

    def modal(self, context, event):
        if event.type == "RIGHTMOUSE" and event.value == "PRESS" :
            bpy.ops.wm.tool_set_by_id(name=self.previous_tool_id)
            if not context.scene.quick_select_keep_overlay : 
                set_overlay_visible(False)
            return {'FINISHED'}
        
        return {'PASS_THROUGH'}

    def execute(self, context):
        return self.invoke(context, None)

    def invoke(self, context, event):
        print("Quick select activated.")

        context.scene.tool_settings.use_gpencil_select_mask_stroke = True

        pop = lambda l : l[0] if len(l) > 0 else None
        current_tool = pop([e for e in bpy.context.workspace.tools if e.mode == bpy.context.object.mode])
        assert current_tool is not None, "ERROR : no active current tool found."

        print("Storing tool : ", current_tool.idname)
        self.previous_tool_id = current_tool.idname

        context.scene.tool_settings.use_gpencil_select_mask_stroke
        bpy.ops.wm.tool_set_by_id(name='builtin.select_lasso')
        set_overlay_visible(True)

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

class AAT_OT_clear_frame(bpy.types.Operator) :
    bl_idname = "gpencil.clear_frame"
    bl_label = "Clear Current Frame"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        pop = lambda l : l[0] if len(l) > 0 else None
        frame_n = context.scene.frame_current
        gp_objects = [o for o in bpy.context.selected_objects if type(o.data).__name__ == "GreasePencil"]

        for gp in gp_objects :
            for l in gp.data.layers : 
                frame = pop([f for f in l.frames if f.frame_number == frame_n])

                if frame is not None : 
                    print(f"Clearing {gp.name}.{l.info}@{frame_n}")
                    for s in reversed(frame.strokes) : 
                        frame.strokes.remove(s)
                
                else : 
                    print(f"Creating {gp.name}.{l.info}@{frame_n}")
                    l.frames.new(frame_n)
            
            boolean.refresh_geometry_display(object=gp)
        
        depsgraph = bpy.context.evaluated_depsgraph_get()
        depsgraph.update()
        
        blender_ui.full_redraw()
        
        return {'FINISHED'}

class AAT_OT_QuickTransform(bpy.types.Operator) :
    bl_idname = "gpencil.quick_transform"
    bl_label = "Quick Transform"
    bl_options = {'REGISTER', 'UNDO'}    # Operator Variables
    transform_type = "NONE" 

    def modal(self, context, event):
        """Handle user inputs in the modal state"""
        
        if event.type == 'RIGHTMOUSE':
            if event.value == 'RELEASE':
                self.lattice.apply()
                self.lattice.clear()
                self.previous_mode.switch(restore_active_object=True)
                return {"FINISHED"}
        
        if event.type == 'LEFTMOUSE':
            if event.value == 'RELEASE':
                self.transform_type = 'NONE'

            elif event.value == 'PRESS':
                self.reset_start_position(event)
                print(event.ctrl, event.alt)

                if event.ctrl:
                    self.transform_type = "ROTATE"
                elif event.shift:
                    self.transform_type = "SCALE"
                else:
                    self.transform_type = "TRANSLATE"
        
        elif event.type == 'MOUSEMOVE' :
            try : 
                if self.transform_type == "TRANSLATE" : 
                    self.translate(event)
                elif self.transform_type == "ROTATE" : 
                    self.rotate(event)
                elif self.transform_type == "SCALE" : 
                    self.scale(event)
            
            except Exception as e :
                self.lattice.clear()
                raise e

        return {'RUNNING_MODAL'}
    
    def translate(self, event) : 
        mouse_position = mathutils.Vector((event.mouse_region_x, event.mouse_region_y))
        depth_coord = self.start_points[0]
        delta_mouse_3D = geo3D.region_to_location(mouse_position, depth_coord) - geo3D.region_to_location(self.start_mouse, depth_coord)
        delta_mouse_3D = np.array(delta_mouse_3D)

        new_points = [p+delta_mouse_3D for p in self.start_points]
        self.set_points(new_points)

    def rotate(self, event) : 
        mouse_position = mathutils.Vector((event.mouse_region_x, event.mouse_region_y))

        pivot = np.array(self.start_mouse)
        dp = np.array(mouse_position)-pivot
        angle = (dp[1]*.002)%(math.pi*2)

        # Rotation matrix in 2D space
        rot = np.array([
            [np.cos(angle), -np.sin(angle)], 
            [np.sin(angle),  np.cos(angle)]
        ])

        points = []
        for p in self.start_points :
            pv = mathutils.Vector(p.tolist())
            w = np.array(geo3D.location_to_region(pv)) - pivot
            w = rot @ w
            new_pos = w + pivot
            new_pos = geo3D.region_to_location(mathutils.Vector(new_pos.tolist()), self.start_points[0])
            points.append(np.array(new_pos))
        
        self.set_points(points)

    def scale(self, event) : 
        mouse_position = mathutils.Vector((event.mouse_region_x, event.mouse_region_y))

        pivot = np.array(self.start_mouse)
        dp = np.array(mouse_position)-pivot
        scale = 1+dp[1]*.01


        points = []
        for p in self.start_points :
            pv = mathutils.Vector(p.tolist())
            w = np.array(geo3D.location_to_region(pv)) - pivot
            w = w*scale
            new_pos = w + pivot
            new_pos = geo3D.region_to_location(mathutils.Vector(new_pos.tolist()), self.start_points[0])
            points.append(np.array(new_pos))
        
        self.set_points(points)

    def get_points(self) : 
        points = []
        matrix_world = self.lattice.cage.matrix_world
        for p in self.lattice.lattice.points : 
            co = geo3D.to_world(np.array(p.co_deform), matrix_world)
            points.append(co)
        return points
    
    def set_points(self, points) : 
        matrix_world = self.lattice.cage.matrix_world
        for i in range(0,len(points)) : 
            co = geo3D.to_local(points[i], matrix_world)
            self.lattice.lattice.points[i].co_deform = mathutils.Vector(co.tolist())

    def reset_start_position(self, event) : 
        self.start_mouse = mathutils.Vector((event.mouse_region_x, event.mouse_region_y))
        self.start_points = self.get_points()

    def invoke(self, context, event):
        """Set up modal mode and initialize stroke data"""
        if not context.object or context.object.type != 'GPENCIL':
            self.report({'WARNING'}, "Active object is not a Grease Pencil object")
            return {'CANCELLED'}
        
        self.previous_mode = mode_manager.switch_mode("EDIT_GPENCIL")
        
        self.lattice = LatticeControl(context.object)
        self.lattice.create_lattice()

        self.reset_start_position(event)
        set_overlay_visible(False)
        
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}




class AAT_OT_ewi_scene_cleanup(bpy.types.Operator) :
    bl_idname = "anim.ewi_scene_cleanup"
    bl_label = "EWI Scene Cleanup"
    bl_options = {'REGISTER', 'UNDO'}

    @staticmethod
    def unlink_col(child_col) : 
        all_collections = [c for c in bpy.data.collections] + [bpy.context.scene.collection]
        for col in all_collections:
            for c in col.children : 
                if c.name == child_col.name : 
                    print("Unlinking %s from %s" % (c.name, col.name))
                    col.children.unlink(c)
    @staticmethod
    def move_col_to(child_col, parent_col) : 
        AAT_OT_ewi_scene_cleanup.unlink_col(child_col)
        parent_col.children.link(child_col)

    def invoke(self, context, event):

        col_anim = bpy.data.collections["COL_ANIM"]

        for c in bpy.context.scene.collection.children : 

            if "COL-RDR_CAM" in c.name and len(c.all_objects) == 0 : 
                print("Deleting %s" % c.name)
                self.unlink_col(c)
            
            if c.name.startswith(".") : 
                print("Moving %s to COL_ANIM" % c.name)
                self.move_col_to(c, col_anim)
        
        return {'FINISHED'}
    
    def execute(self, context):
        return self.invoke(context, None)

