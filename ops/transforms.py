import bpy

from ..common.gp_utils import get_active_gp_object, select_all_gp_geometry
from ..common.mode_manager import switch_mode
from ..common.blender_ui import show_message

from ..core.lattice_control import LatticeControl
from ..core.UI_elements import QuickEditWidget


class AAT_OT_lattice_deform(bpy.types.Operator) :
    bl_idname = "anim.lattice_deform"
    bl_label = "Lattice deform in Grease Pencil"
    bl_options = {'REGISTER', 'UNDO'}

    _timer = None

    n_lattice = 2


    def exit(self) :
        self.lattice.clear()
        self.previous_mode.switch(restore_active_object=True)
        self.obj.select_set(True)


    def modal(self, context, event):
        context.area.tag_redraw()
        
        if event.type in ["RET", "NUMPAD_ENTER"] and event.value == "PRESS" :
            self.lattice.apply()
            self.exit()
            return {"FINISHED"}
        
        if event.type == "ESC" and event.value == "PRESS" :
            self.exit()
            return {"FINISHED"}
        
        if event.type == "NUMPAD_PLUS" and event.value == "PRESS" :
            self.n_lattice += 1
            self.lattice.update_lattice_dimensions((self.n_lattice, self.n_lattice))
            return {'RUNNING_MODAL'}
        
        if event.type == "NUMPAD_MINUS" and event.value == "PRESS" :
            if self.n_lattice > 2 :
                self.n_lattice -= 1
            self.lattice.update_lattice_dimensions((self.n_lattice, self.n_lattice))
            return {'RUNNING_MODAL'}
        
        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        
        obj = get_active_gp_object()
        self.obj = obj
        
        if obj is None :
            self.report({'ERROR'}, "/!\\ Box Deform : Cannot find object to target")
            return {'CANCELLED'}
    
        if context.area.type != 'VIEW_3D':
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return {'CANCELLED'}
        
        if bpy.context.mode == "PAINT_GPENCIL" :
            show_message(title="Quick Edit unavailable", message="Please go to Edit Mode and select elements to do transforms")
            return {'CANCELLED'}
        
        if bpy.context.mode == "OBJECT" :
            select_all_gp_geometry(obj)

        self.previous_mode = switch_mode("EDIT_GPENCIL")
        
        lattice = LatticeControl(obj)
        lattice.create_lattice((self.n_lattice, self.n_lattice))

        self.lattice = lattice

        context.window_manager.modal_handler_add(self)

        return {'RUNNING_MODAL'}
    



class AAT_OT_quick_edit(bpy.types.Operator) :
    bl_idname = "anim.quick_edit"
    bl_label = "Quickly deform in Grease Pencil"
    bl_options = {'REGISTER', 'UNDO'}

    _timer = None

    rotating = None

    def exit(self) :
        self.lattice.clear()
        self.widget.delete()
        self.previous_mode.switch(restore_active_object=True)
        self.obj.select_set(True)

    def modal(self, context, event):
        context.area.tag_redraw()

        try :

            self.widget.on_modal(context, event)
            
            if event.type in ["RET", "NUMPAD_ENTER"] and event.value == "PRESS" :
                self.lattice.apply()
                self.exit()
                return {"FINISHED"}
            
            if event.type == "ESC" and event.value == "PRESS" :
                self.exit()
                return {"FINISHED"}
            
            if event.type == "LEFT_SHIFT" or event.type == "RIGHT_SHIFT" :
                if event.value == "PRESS" :
                    self.widget.conserve_aspect_ratio = True
                elif event.value == "RELEASE" :
                    self.widget.conserve_aspect_ratio = False
            
            if event.type == "LEFT_ALT" or event.type == "RIGHT_ALT" :
                if event.value == "PRESS" :
                    self.widget.central_symmetry = True
                elif event.value == "RELEASE" :
                    self.widget.central_symmetry = False
        
        except Exception as e :
            self.lattice.clear()
            raise e

        
        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        
        obj = get_active_gp_object()
        self.obj = obj
        
        if obj is None :
            self.report({'ERROR'}, "/!\\ Box Deform : Cannot find object to target")
            return {'CANCELLED'}
    
        if context.area.type != 'VIEW_3D':
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return {'CANCELLED'}
        
        if bpy.context.mode == "PAINT_GPENCIL" :
            show_message(title="Quick Edit unavailable", message="Please go to Edit Mode and select elements to do transforms")
            return {'CANCELLED'}
        
        if bpy.context.mode == "OBJECT" :
            select_all_gp_geometry(obj)

        self.previous_mode = switch_mode("EDIT_GPENCIL")
        
        lattice = LatticeControl(obj)
        lattice.create_lattice()

        self.lattice = lattice

        self.widget = QuickEditWidget(context, lattice)

        context.window_manager.modal_handler_add(self)

        return {'RUNNING_MODAL'}
