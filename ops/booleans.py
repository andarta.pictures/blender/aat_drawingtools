import bpy

from ..common.blender_ui import show_message
from ..common import gp_utils

from ..core import boolean, dynamic_boolean, knife_tool


def base_lasso_modal(context, event, lasso_UI, on_apply) :
    lasso_UI.update_cursor()

    context.area.tag_redraw()
    
    if event.type == "LEFT_CTRL" or event.type == "RIGHT_CTRL" :
        if event.value == "PRESS" :
            print("Changing mode to ADD")
            lasso_UI.set_mode("ADD")
        
        if event.value == "RELEASE" :
            print("Changing mode to REMOVE")
            lasso_UI.set_mode("REMOVE")

    if event.type == "LEFTMOUSE" and event.value == "PRESS" : 
        lasso_UI.drawing = True
    
    if lasso_UI.mode == "ADD" and event.type == "Z" and event.value == "PRESS" :
        try :
            bpy.ops.ed.undo()
        except RuntimeError :
            print("Undo failed.")

    if event.type == "MIDDLEMOUSE" :
            return {'PASS_THROUGH'}

    if event.type in ["RET", "NUMPAD_ENTER", "ESC", "RIGHTMOUSE", "MIDDLEMOUSE"] and event.value == "PRESS" and not lasso_UI.drawing : 
        lasso_UI.delete()
        context.window.cursor_set("DEFAULT")
        if event.type == "MIDDLEMOUSE" :
            return {'PASS_THROUGH'}
        return {'FINISHED'}


    if not lasso_UI.drawing :
        return {'RUNNING_MODAL'}

    if event.type == "LEFTMOUSE" and event.value == "RELEASE" : 
        on_apply()            
        return {'RUNNING_MODAL'}
    
    else :
        mouse_position = (event.mouse_region_x, event.mouse_region_y)
        lasso_UI.add_position(mouse_position)
        return {'RUNNING_MODAL'}
    
def base_lasso_init(context, event) :

    lasso_UI = boolean.Lasso(context)

    if event.ctrl :
        lasso_UI.mode = "ADD"

    context.window.cursor_set("KNIFE")

    return lasso_UI


class AAT_OT_gp_potato_tool(bpy.types.Operator) :
    """Cut shapes and holes into an existing grease pencil. This tool can the number of 
    points for maximum drawing precision, or preserve the amount of points to keep the 
    interpolation working. Use Ctrl to remove material. """
    
    bl_idname = "anim.gp_potato_tool"
    bl_label = "Grease Pencil Potato tool"
    bl_options = {'REGISTER', 'UNDO'}

    lasso_UI : boolean.Lasso

    def apply_cut(self):
        addon_name = __package__.split(".")[0]
        print("Addon name : ", addon_name)
        prefs = bpy.context.preferences.addons[addon_name].preferences

        if prefs.preserve_topo :
            path = self.lasso_UI.get_path()
            mode = self.lasso_UI.get_mode()
            knife_tool.knife_cut(path, mode)
        
        else :
            dynamic_boolean.apply_potato(self.lasso_UI.get_path(), self.lasso_UI.get_mode())

        boolean.refresh_geometry_display()

    def conclude(self):
        try :
            self.apply_cut()
        
        finally :
            self.lasso_UI.reset_path()
            
        self.lasso_UI.drawing = False

        bpy.ops.ed.undo_push(message = "Boolean cut")

    def modal(self, context, event):
        if event.type == "LEFT_CTRL" or event.type == "RIGHT_CTRL" :

            if event.value == "PRESS" :
                self.lasso_UI.set_mode("REMOVE")
            
            if event.value == "RELEASE" :
                self.lasso_UI.set_mode("ADD")

            return {'RUNNING_MODAL'}
        
        if event.type == "Z" and event.value == "PRESS" :
            if self.lasso_UI.get_mode() == "REMOVE" :
                try :
                    bpy.ops.ed.undo()
                
                except RuntimeError :
                    print("Undo failed.")

            return {'RUNNING_MODAL'}

        return base_lasso_modal(context, event, self.lasso_UI, self.conclude)

    def invoke(self, context, event):
        if not dynamic_boolean.shapely_available :
            show_message("Knife paint needs shapely to work, please install it first.", "Shapely missing", "ERROR")
            return {"CANCELLED"}

        self.lasso_UI = base_lasso_init(context, event)
    
        if event.ctrl :
            self.lasso_UI.set_mode("REMOVE")
        else :
            self.lasso_UI.set_mode("ADD")

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}
    
class AAT_OT_layer_boolean(bpy.types.Operator) :
    """Layer boolean"""
    bl_idname = "anim.layer_boolean"
    bl_label = "Layer Boolean"
    bl_options = {'REGISTER', 'UNDO'}

    mode : bpy.props.StringProperty(default="UNION")

    def get_next_layer(self, gp_obj, layer) :
        layer_list = [l for l in reversed(gp_obj.data.layers)]
        layer_name_list = [l.info for l in layer_list]

        next_layer_id = layer_name_list.index(layer.info)+1

        if next_layer_id < len(layer_list) :
            return layer_list[next_layer_id]


    def invoke(self, context, event):
        gp_obj = gp_utils.get_active_gp_object()
        layer = boolean.get_active_layer(gp_obj)

        if layer is None :
            show_message("Invalid layer.")
            return {'CANCELLED'}

        if self.mode == "UNION" :
            dynamic_boolean.layer_union(layer, gp_obj)
        
        else :
            next_layer = self.get_next_layer(gp_obj, layer)
            if next_layer is None :
                show_message("This tool needs a layer below this one to function.")
                return {'CANCELLED'}

            if self.mode == "DIFF" :
                dynamic_boolean.layer_difference(next_layer, layer, gp_obj)

            if self.mode == "INTER" :
                dynamic_boolean.layer_intersection(next_layer, layer, gp_obj)

        
        return {'FINISHED'}
    

class AAT_OT_layer_union(AAT_OT_layer_boolean) :
    """Simplify and merges all the strokes in the currently selected layer"""
    bl_idname = "anim.layer_union"
    bl_label = "Layer Union"
    bl_options = {'REGISTER', 'UNDO'}

    def __init__(self) :
        super().__init__()
        self.mode = "UNION"
    

class AAT_OT_layer_intersection(AAT_OT_layer_boolean) :
    """Replaces the layer below the selected one with its intersection with the selected layer"""
    bl_idname = "anim.layer_intersection"
    bl_label = "Layer Intersection"
    bl_options = {'REGISTER', 'UNDO'}

    def __init__(self) :
        super().__init__()
        self.mode = "INTER"
    

class AAT_OT_layer_difference(AAT_OT_layer_boolean) :
    """Replaces the layer below the selected one with its difference with the selected layer"""
    bl_idname = "anim.layer_difference"
    bl_label = "Layer Difference"
    bl_options = {'REGISTER', 'UNDO'}

    def __init__(self) :
        super().__init__()
        self.mode = "DIFF"