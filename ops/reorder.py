import bpy
import numpy as np

from ..common.gp_utils import get_active_gp_object, select_all_gp_geometry
from ..common.utils import register_classes, unregister_classes
from ..common.keymaps import register_keymap
from ..common.mode_manager import switch_mode
from ..common.blender_ui import show_message
from ..common import gp_utils, blender_ui

from ..core.lattice_control import LatticeControl
from ..core import boolean, dynamic_boolean, knife_tool, zpush
from ..core.UI_elements import QuickEditWidget, PerspectiveGrid


zpush_state = {
    "display_location" : (250, 75),
    "text_size" : 12,
    "autoscale" : True,
}

current_display_layout = None


class GPEDIT_OT_edit_gp_depth(bpy.types.Operator):
    bl_idname = 'anim.edit_gp_depth'
    bl_label = 'Dialog'
    bl_options = {'REGISTER', 'UNDO'}
    
    depth : bpy.props.FloatProperty( default = 0.0, min = -10000.0, max = 10000.0, step = 1, precision = 3 )
    objects : bpy.props.StringProperty()

    def execute(self, context):
        print ("Manually changing depth for %s to %.3f"%(self.objects, self.depth))
        gp_objects = self.objects.split(",")

        selection = current_display_layout.get_selected_elements()
        if len(selection) > 0 :
            gp_objects = [e.text for e in selection]

        for o_name in gp_objects :
            obj = bpy.data.objects[o_name]
            zpush.zpush_gp(obj, self.depth, autoscale=current_display_layout.autoscale)
            current_display_layout.update_rounded_status()
        
        current_display_layout.update_display()
        blender_ui.full_redraw()
        bpy.ops.ed.undo_push(message="Manually edited %s depth."%self.objects)   
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self, width = 200)

    def draw(self, context):
        layout = self.layout
        layout.prop(self, 'depth', text = 'Depth')

class GPEDIT_OT_gp_reorder(bpy.types.Operator):
    """Reorder GreasePencil objects."""
    bl_idname = "anim.gp_reorder"
    bl_label = "Reorder GreasePencil"
    bl_options = {'REGISTER', 'UNDO'}
    
    mode_mandatory_params = {
        "ZOOM" : ["start_mouse_position", "base_text_size"],
        "PAN" : ["start_mouse_position"],
        "DEPTHSCRUB" : ["start_mouse_position", "target_objects"],
        "DRAGNDROP" : ["moved_object", "selected_objects", "start_mouse_position"],
    }


    def start_mode(self, mode_name, **params) :
        assert mode_name in self.mode_mandatory_params.keys(), "Invalid mode name"
        
        mandatory_params = self.mode_mandatory_params[mode_name]
        assert set(mandatory_params) == set(params.keys()), "Invalid params : %s needed."%str(mandatory_params)

        self.mode = mode_name
        for k in params :
            self.data[k] = params[k]


    def clear_mode(self, *modes) :
        if len(modes) > 0 :
            if self.mode not in modes :
                return
                
        param_names = self.mode_mandatory_params[self.mode]
        for p in param_names :
            del self.data[p]

        self.mode = "DEFAULT"


    def zoom_modal(self, event, mouse_position) :
        if event.type == 'MOUSEMOVE' : 
            d = np.array(mouse_position)-np.array(self.data["start_mouse_position"])
            new_size = int(self.data["base_text_size"] * (1+(d[0]+d[1]) * 0.001))
            self.display_layout.text_size = new_size
            self.display_layout.update_display()
            blender_ui.full_redraw()

        if event.type == "MIDDLEMOUSE" and event.value == "RELEASE" : 
            self.clear_mode()


    def pan_modal(self, event, mouse_position) :
        if event.type == 'MOUSEMOVE' : 
            d = np.array(mouse_position)-np.array(self.data["start_mouse_position"])
            self.display_layout.displace_all(d)
            self.data["start_mouse_position"] = mouse_position
            blender_ui.full_redraw()

        if event.type == "MIDDLEMOUSE" and event.value == "RELEASE" : 
            self.clear_mode()
    

    def dragndrop_modal(self, event, mouse_position) :
        if event.type == 'MOUSEMOVE' : 
            x,y = mouse_position
            moved_object = self.data["moved_object"]
            moved_object.set_coordinates(x,y)
            blender_ui.full_redraw()

        if event.type == "RIGHTMOUSE" and event.value == "PRESS" : 
            self.clear_mode()
            self.display_layout.update_display()
            blender_ui.full_redraw()
        
        if event.type == "LEFTMOUSE" and event.value == "RELEASE" : 
            distance = np.linalg.norm(np.array(mouse_position) - np.array(self.data["start_mouse_position"]))
            print("Drop distance : ", distance)
            element = self.data["moved_object"]

            if distance < element.get_box_dimensions()[1] :
                if distance == 0.0 :
                    self.display_layout.toggle_selection(element)
            
            else :
                displacement = self.display_layout.element_dropped(self.data["moved_object"], mouse_position)

                if len(self.data["selected_objects"]) > 0 :
                    for e in self.data["selected_objects"] :
                        if e.text != self.data["moved_object"].text :
                            gp = bpy.data.objects[e.text]
                            zpush.zpush_gp(gp, gp.location.y+displacement, autoscale=self.display_layout.autoscale)
                            self.display_layout.update_rounded_status()

                bpy.ops.ed.undo_push(message="Changed %s depth." % self.data["moved_object"].text)
            
            self.clear_mode()
            self.display_layout.update_display()
            blender_ui.full_redraw()


    def depthscrub_modal(self, event, mouse_position) :
        if event.type == 'MOUSEMOVE' : 
            gp_objects = self.data['target_objects']
            
            ratio = -0.0005
            if self.shift and self.ctrl :
                ratio *= 100
            if self.shift : 
                ratio *= 0.1
            elif self.ctrl : 
                ratio *= 10

            d = np.array(mouse_position)-np.array(self.data["start_mouse_position"])
            depth_displacement = d[1]*ratio
            
            if abs(d[1]) < self.region.height*0.9 :
                for gp in gp_objects :
                    current_depth = gp.location.y
                    zpush.zpush_gp(gp, current_depth+depth_displacement, autoscale=self.display_layout.autoscale)

            self.display_layout.update_display()

            mouse_position = blender_ui.cycle_mouse(mouse_position, region=self.region)

            self.data["start_mouse_position"] = mouse_position
            blender_ui.full_redraw()

        if event.type == "LEFTMOUSE" and event.value == "RELEASE" : 
            bpy.ops.ed.undo_push(message="Changed %s depth." % str([o.name for o in self.data["target_objects"]]))
            self.display_layout.update_rounded_status()
            self.clear_mode()


    def default_modal(self, event, mouse_position) : 
        if event.type == 'MOUSEMOVE' : 
            self.display_layout.mouse_react(mouse_position)
            
            blender_ui.full_redraw()
        
        if event.type == "MIDDLEMOUSE" and  event.value == "PRESS" : 
            if self.ctrl :
                size = self.display_layout.text_size
                self.start_mode("ZOOM", start_mouse_position=mouse_position, base_text_size=size)
            else :
                self.start_mode("PAN", start_mouse_position=mouse_position)
            
        if event.type == "LEFTMOUSE" and event.value == "PRESS" : 
            display_element = self.display_layout.detect_collision(mouse_position)

            if display_element is not None :
                self.on_element_click(display_element, mouse_position)
            else :
                print("No collision detected.")

        if event.type == "RIGHTMOUSE" and event.value == "PRESS" : 
            display_element = self.display_layout.detect_collision(mouse_position)

            if display_element is not None :
                if display_element.type == "DEPTH" :
                    objects = display_element.linked_objects
                elif display_element.type == "LAYER" :
                    objects = [display_element.text]
                else :
                    objects = None

                if objects is not None :
                    obj = bpy.data.objects[objects[0]]
                    object_list = ",".join(objects)
                    bpy.ops.anim.edit_gp_depth('INVOKE_DEFAULT', depth=obj.location.y, objects=object_list)  
            else :
                print("Right click on empty space, exiting.")
                self.conclude()
                return {'FINISHED'}

        if event.type == "A" and event.value == "PRESS" : 
            for e in self.display_layout.display_elements :
                if self.alt :
                    e.selected = False
                else :
                    e.selected = True

            self.display_layout.update_blender_selection()
            blender_ui.full_redraw()



    def always_modal(self, event, mouse_position) :
        if event.type in ["ESC", "R"] and event.value == "PRESS" : 
            self.conclude()
            return {'FINISHED'}
        
        if event.type == "Z" and event.value == "PRESS" :
            try :
                if self.ctrl and self.shift :
                    bpy.ops.ed.redo()
                elif self.ctrl :
                    bpy.ops.ed.undo()
                
                self.display_layout.update_display()
                blender_ui.full_redraw()

            except RuntimeError :
                print("Undo/redo failed.")
            
            return {'RUNNING_MODAL'}
        
        if event.type in ["LEFT_CTRL", "RIGHT_CTRL"] : 
            if event.value == "PRESS" :
                self.ctrl = True
            if event.value == "RELEASE" : 
                self.ctrl = False
            
        if event.type in ["LEFT_SHIFT", "RIGHT_SHIFT"] : 
            if event.value == "PRESS" :
                self.shift = True
            if event.value == "RELEASE" : 
                self.shift = False
            
        if event.type in ["LEFT_ALT", "RIGHT_ALT"] : 
            if event.value == "PRESS" :
                self.alt = True
            if event.value == "RELEASE" : 
                self.alt = False
        

    def on_element_click(self, display_element, mouse_position) :
        if self.shift and display_element.type == "LAYER" :
            self.display_layout.toggle_selection(display_element)
            return 
        
        if display_element.type == "LAYER" :
            print("Collision detected with ", display_element.text)
            display_element.dragged = True
            selection = self.display_layout.get_selected_elements()
            self.start_mode("DRAGNDROP", moved_object = display_element, selected_objects=selection, start_mouse_position=mouse_position)
        
        elif display_element.type == "ARROW" :
            if self.ctrl and self.shift :
                nudge = -1
            elif self.shift :
                nudge = -0.001
            elif self.ctrl :
                nudge = -0.1
            else :
                nudge = -0.01
            
            if display_element.text == "▾" : 
                nudge *= -1
            
            print("Moving %s by %.2f"%(display_element.linked_objects, nudge))

            gp_objects = [bpy.data.objects[o_name] for o_name in display_element.linked_objects]

            selected = self.display_layout.get_selected_elements()
            if len(selected) > 0 :
                for selected_element in selected :
                    if selected_element.text in display_element.linked_objects :
                        gp_objects = [bpy.data.objects[e.text] for e in selected]
            
            for gp in gp_objects :
                zpush.zpush_gp(gp, gp.location.y+nudge, autoscale=self.display_layout.autoscale)
                self.display_layout.update_rounded_status()
            
            bpy.ops.ed.undo_push(message="Changed %s depth." % str(display_element.linked_objects))

            self.display_layout.update_display()
            blender_ui.full_redraw()
        
        elif display_element.type == "AUTOSCALE_OPTION" : 
            self.display_layout.autoscale = not self.display_layout.autoscale
            self.display_layout.update_display()
            blender_ui.full_redraw()
        
        elif display_element.type == "ZFIGHT_CHECK_OPTION" : 
            self.display_layout.zfight_check()
            self.display_layout.update_display()
            blender_ui.full_redraw()

        elif display_element.type == "ROUNDVALUES_OPTION" :
            print("Rounding values !")
            gp_objects = [bpy.data.objects[o_name] for o_name in self.display_layout.gp_objects]
            
            for gp in gp_objects :
                new_depth = round(gp.location.y, 3)
                gp.location.y = new_depth

            self.display_layout.update_rounded_status()
            self.display_layout.update_display()
            blender_ui.full_redraw()
            bpy.ops.ed.undo_push(message="Rounded objects depth.")
        
        elif display_element.type == "DEPTH" :
            gp_objects = [bpy.data.objects[o_name] for o_name in display_element.linked_objects]
            
            selected = self.display_layout.get_selected_elements()
            if len(selected) > 0 :
                for selected_element in selected :
                    if selected_element.text in display_element.linked_objects :
                        gp_objects = [bpy.data.objects[e.text] for e in selected]

            self.start_mode("DEPTHSCRUB", start_mouse_position=mouse_position, target_objects=gp_objects)

        
    def modal(self, context, event) :
        try :
        
            mouse_position = (event.mouse_region_x, event.mouse_region_y)
            self.context = context

            ret = self.always_modal(event, mouse_position)
            if ret is not None :
                return ret
            
            if self.mode == "ZOOM" :
                self.zoom_modal(event, mouse_position)

            elif self.mode == "PAN" :
                self.pan_modal(event, mouse_position)
            
            elif self.mode == "DRAGNDROP" :
                self.dragndrop_modal(event, mouse_position)
            
            elif self.mode == "DEPTHSCRUB" :
                self.depthscrub_modal(event, mouse_position)
            
            else :
                ret = self.default_modal(event, mouse_position)
                if ret is not None :
                    return ret

            return {'RUNNING_MODAL'}
        
        except Exception as e :
            self.conclude()
            raise e

    

    def conclude(self) :
        global zpush_state

        if 'display_layout' in dir(self) :
            zpush_state = {
                "display_location" : self.display_layout.base_position,
                "text_size" : self.display_layout.text_size,
                "autoscale" : self.display_layout.autoscale,
            }

            self.display_layout.destroy_display_elements()
        
        blender_ui.full_redraw()
        bpy.context.window.cursor_modal_restore()


    def execute(self, context):
        global current_display_layout, zpush_state

        def valid_object(o) :
            conditions = [
                type(o.data).__name__ in ["GreasePencil", "Mesh"],
                not o.hide_get(),
                not gp_utils.is_excluded_from_viewlayer(o),
            ]
            return False not in conditions


        try :
            objects = [o for o in context.scene.objects if valid_object(o)]

            selected_objects = [o for o in objects if o.select_get()]
            if len(selected_objects) > 1 :
                objects = selected_objects

            o_print = lambda o : "<%s, %s>" % (o.name, type(o.data).__name__)
            print("Reordering : ", ", ".join([o_print(o) for o in objects]))

            self.region = context.region
            print("Initiating zpush with state : ", zpush_state)
            self.display_layout = zpush.Reorder_Layout(objects, zpush_state["display_location"], self.region, text_size=zpush_state["text_size"])
            self.display_layout.autoscale = zpush_state["autoscale"]
            current_display_layout = self.display_layout
            display_height = self.display_layout.update_display()

            highest_y = self.display_layout.base_position[1]+display_height
            print("Highest point : ", highest_y)
            if highest_y < 0 :
                print("Adding %d to base position"%(-highest_y+200))
                x,y = self.display_layout.base_position
                y += -highest_y+200
                self.display_layout.base_position = (x,y)
                self.display_layout.update_display()

            bpy.context.window.cursor_set("DEFAULT")

            self.mode = "DEFAULT"
            self.ctrl = False
            self.shift = False
            self.alt = False
            self.data = {}

            self.display_layout.update_blender_selection()

            blender_ui.full_redraw()
            
            wm = context.window_manager
            wm.modal_handler_add(self)
            return {'RUNNING_MODAL'}
        
        except Exception as e :
            self.conclude()
            raise e